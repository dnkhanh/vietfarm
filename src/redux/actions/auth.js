export const LOGIN = {
  REQUEST: 'LOGIN_REQUEST',
  SUCCESS: 'LOGIN_SUCCESS',
  FAILED: 'LOGIN_FAILED',
  ISLOGIN: 'LOGIN_ISLOGIN',
};
export const LOGOUT = {
  REQUEST: 'LOGOUT_REQUEST',
};
export const REGISTER = {
  REQUEST: 'USER_REGISTER_REQUEST',
  AUTHPHONE:'USER_REGISTER_AUTH_PHONE',
};
export const DATA = {
  AUTHPHONE: 'DATA_AUTHPHONE',
};
export const loginSubmit = (username, password) => ({
  type: LOGIN.REQUEST,
  username,
  password,
});

export const loginSuccess = (response) => ({
  type: LOGIN.SUCCESS,
  response,
});
export const DataAuthPhone = (data) => ({
  type: DATA.AUTHPHONE,
  data,
})
export const loginFailed = (error) => ({
  type: LOGIN.FAILED,
  error,
});

export const userlogOut = (redirect = false) => ({
  type: LOGOUT.REQUEST,
  redirect,
});
export const userReg = (username, password, phone) => ({
  type: REGISTER.REQUEST,
  username,
  password,
  phone,
});
export const authPhone = (phone) => ({
  type: REGISTER.AUTHPHONE,
  phone
});
//check islogin
export const isLogin = (data) => ({
  type: LOGIN.ISLOGIN,
  data,
});
