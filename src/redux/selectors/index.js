// another
export const isLoading = (state) => state.another.isLoading;

export const UserState = (state) => state.auth.infoUser;

export const LangState = (state) => state.another.languageApp;

export const Modal = (state) => state.another.Modal;

export const popupError = (state) => state.another.popupError;

export const popupSuccess = (state) => state.another.popupSuccess;

export const routeName = (state) => state.another.routeName;
