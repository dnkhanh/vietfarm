import {fork, all} from 'redux-saga/effects';
import another from './another';
import auth from './auth';
export default function* rootSaga() {
  yield all([fork(another), fork(auth)]);
}
