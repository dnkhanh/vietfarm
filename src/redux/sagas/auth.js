import {takeLatest, put} from 'redux-saga/effects';
import {LOGIN, LOGOUT, REGISTER, loginSuccess} from '~/redux/actions/auth';
import {
  hideLoading,
  showLoading,
  NavigationReplace,
  showModal,
} from '~/redux/actions/anotherActions';
import {getString, setLocale} from '~/Localizations/i18n';
import {TYPE_MODAL_DANGER} from '~/AppValues';
import auth from '@react-native-firebase/auth';
function* loginSaga(action) {
  try {
    yield put(showLoading());
    // const result = yield auth().signInWithEmailAndPassword(
    //   action.username,
    //   action.password,
    // );
    yield put(loginSuccess({}));
    yield put(hideLoading());
    yield put(NavigationReplace('Main'));
  } catch (error) {
    console.log(error);
    yield put(hideLoading());
    if (error.code == 'auth/user-not-found') {
      yield put(
        showModal(getString('login.error_username_2'), TYPE_MODAL_DANGER),
      );
    } else if (error.code == 'auth/invalid-email') {
      yield put(
        showModal(getString('login.error_username_3'), TYPE_MODAL_DANGER),
      );
    } else {
      yield put(showModal(getString('login.error_pass_1'), TYPE_MODAL_DANGER));
    }
  }
}
function* logoutSaga(action) {
  try {
    yield put(showLoading());
    // yield auth().signOut();
    yield put(hideLoading());
    if (action.redirect) {
      yield put(NavigationReplace('Login'));
    }
  } catch (error) {
    yield put(hideLoading());
  }
}
function* isloginSaga(action) {
  try {
    if (action.data) {
      yield put(showLoading());
      yield put(NavigationReplace('Main'));
      yield put(hideLoading());
    }
  } catch (error) {
    yield put(hideLoading());
  }
}
//Register
function* userRegSaGa(action) {
  try {
    yield put(showLoading());
    // yield auth().createUserWithEmailAndPassword(
    //   action.username,
    //   action.password,
    // );
    // yield put(showModal('Bạn đã đăng ký thành công!','success'));
    yield put(NavigationReplace('Main'));
    yield put(hideLoading());
  } catch (error) {
    yield put(hideLoading());
    if (error.code === 'auth/email-already-in-use') {
      yield put(
        showModal(getString('register.error_mail_3'), TYPE_MODAL_DANGER),
      );
    }
  }
}
export default function* authSaga() {
  yield takeLatest(LOGIN.REQUEST, loginSaga);
  yield takeLatest(LOGOUT.REQUEST, logoutSaga);
  yield takeLatest(REGISTER.REQUEST, userRegSaGa);
  yield takeLatest(LOGIN.ISLOGIN, isloginSaga);
}

/*
import {takeLatest, put} from 'redux-saga/effects';
import {
  LOGIN,
  LOGOUT,
  REGISTER,
  loginSuccess,
  DataAuthPhone,
} from '~/redux/actions/auth';
import {
  hideLoading,
  showLoading,
  NavigationReplace,
  showModal,
} from '~/redux/actions/anotherActions';
import {getString, setLocale} from '~/Localizations/i18n';
import {TYPE_MODAL_DANGER} from '~/AppValues';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

function* loginSaga(action) {
  try {
    yield put(showLoading());
    const result = yield auth().signInWithEmailAndPassword(
      action.username,
      action.password,
    );
    yield put(loginSuccess(result.user));
    yield put(hideLoading());
    yield put(NavigationReplace('Main'));
  } catch (error) {
    // console.log(error);
    yield put(hideLoading());
    if (error.code == 'auth/user-not-found') {
      yield put(
        showModal(getString('login.error_username_2'), TYPE_MODAL_DANGER),
      );
    } else if (error.code == 'auth/invalid-email') {
      yield put(
        showModal(getString('login.error_username_3'), TYPE_MODAL_DANGER),
      );
    } else {
      yield put(showModal(getString('login.error_pass_1'), TYPE_MODAL_DANGER));
    }
  }
}

function* logoutSaga(action) {
  try {
    yield put(showLoading());
    yield auth().signOut();
    yield put(hideLoading());
    if (action.redirect) {
      yield put(NavigationReplace('Login'));
    }
  } catch (error) {
    yield put(hideLoading());
  }
}
function* isloginSaga(action) {
  try {
    if (action.data) {
      yield put(showLoading());
      yield put(NavigationReplace('Main'));
      yield put(hideLoading());
    }
  } catch (error) {
    yield put(hideLoading());
  }
}
//Register

function* authPhone(phone) {
  try {
    yield put(showLoading());
    const result = yield auth().signInWithPhoneNumber(phone);
    yield put(hideLoading());
    yield put(DataAuthPhone(result));
  } catch (error) {
    // yield put(DataAuthPhone(error))
    console.log(error);
  }
}
function* userRegSaGa(action) {
  try {
    yield put(showLoading());
    yield auth().createUserWithEmailAndPassword(
      action.username,
      action.password,
    );
    // yield authPhone('+84973563159');
    yield put(NavigationReplace('Authphone'));
    yield authPhone(action.phone);
    // yield put(showModal('Bạn đã đăng ký thành công!','success'));
    // yield put(NavigationReplace('Main'));
    yield put(hideLoading());
  } catch (error) {
    yield put(hideLoading());
    if (error.code === 'auth/email-already-in-use') {
      yield put(
        showModal(getString('register.error_mail_3'), TYPE_MODAL_DANGER),
      );
    }
  }
}
export default function* authSaga() {
  yield takeLatest(LOGIN.REQUEST, loginSaga);
  yield takeLatest(LOGOUT.REQUEST, logoutSaga);
  yield takeLatest(REGISTER.REQUEST, userRegSaGa);
  yield takeLatest(LOGIN.ISLOGIN, isloginSaga);
}
*/
