import {LOGIN,DATA} from '~/redux/actions/auth';
const initialState = {
  dataAuthPhone:null,
  infoUser: {
    uid: undefined,
    username: undefined,
    emailVerified: undefined,
    phoneNumber: undefined,
    isLogin:false
  },
};
export default function (state = initialState, action) {
  const {type} = action;
  switch (type) {
    case LOGIN.SUCCESS: {
      return {
        infoUser: {
          uid: action.response.uid,
          username: action.response.email,
          emailVerified: action.response.emailVerified,
          phoneNUmber: action.response.honeNumber,
          isLogin: true
        },
      };
    }
    case DATA.AUTHPHONE:{
      return {
        dataAuthPhone: action.data
      }
    }
    default:
      return state;
  }
}
