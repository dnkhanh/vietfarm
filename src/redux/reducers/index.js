import {combineReducers} from 'redux';
import nav from './navReducer';
import another from './another';
import auth from './auth';
const rootReducer = combineReducers({
  nav,
  another,
  auth
});

export default rootReducer;
