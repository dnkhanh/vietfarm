import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {
  COLOR_BG_WHITE,
  COLOR_BG_DARK_GREY,
  COLOR_BG_GREEN,
  COLOR_GRAY,
  COLOR_BG_GREEN_LIGHT,
} from '../../AppValues';
import TextView from '../../components/ViewBasic/TextView';

const cacheHistory = ['Hoa quả sạch', 'Bánh kẹo', 'Thịt lợn mán'];
const dataSuggest = [
  'Hoa quả sạch',
  'Bánh kẹo',
  'Thịt lợn mán',
  'Cơm chay',
  'Trái cây',
];

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={styles.content}>
        <View style={styles.viewHistory}>
          <View style={styles.flexDirection}>
            <TextView h7 style={{color: COLOR_BG_DARK_GREY, flex: 1}}>
              Lịch sử tìm kiếm
            </TextView>
            <TextView h7 style={{color: COLOR_BG_GREEN}}>
              XÓA
            </TextView>
          </View>
          {cacheHistory.map((i) => (
            <TextView h6 key={i} style={{paddingTop: 10}}>
              {i}
            </TextView>
          ))}
        </View>
        <View
          style={{height: 10, backgroundColor: COLOR_GRAY, marginVertical: 10}}
        />
        <View style={styles.viewSuggest}>
          <TextView h7 style={{color: COLOR_BG_DARK_GREY}}>
            Gợi ý
          </TextView>
          <View
            style={[
              styles.flexDirection,
              {flexWrap: 'wrap', paddingVertical: 10},
            ]}>
            {dataSuggest.map((i) => (
              <View key={i} style={styles.itemSuggest}>
                <TextView h5>{i}</TextView>
              </View>
            ))}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flexDirection: {
    flexDirection: 'row',
  },
  viewHistory: {
    paddingTop: 10,
    paddingHorizontal: 10,
  },
  paddingTop: {
    paddingTop: 10,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
  viewSearch: {
    marginTop: 5,
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    paddingBottom: 10,
    borderBottomWidth: 0.5,
  },
  viewSuggest: {
    flex: 1,
    paddingTop: 10,
    paddingHorizontal: 10,
  },
  itemSuggest: {
    borderColor: COLOR_BG_GREEN,
    borderWidth: 1,
    borderRadius: 15,
    paddingHorizontal: 10,
    paddingVertical: 3,
    backgroundColor: COLOR_BG_GREEN_LIGHT,
    margin: 5,
  },
});

export default Search;
