import React, {Component} from 'react';
import {View, FlatList, StyleSheet, TouchableOpacity} from 'react-native';
import {Header, TextView, TextInput} from '~/components';
import {IconSearch, IconX, IconRight} from '~/image';
import TinhThanh from '~/tinhthanh.json';
import {COLOR_GRAY, COLOR_BG_WHITE, COLOR_BG_DARK_GREY} from '~/AppValues';

export default class PlaceSearch extends Component {
  constructor(props) {
    super(props);
    const key = Object.keys(TinhThanh);
    this.listPlace = key.map((i) => ({
      code: i,
      ...TinhThanh[i],
      nameSearch: this.change_alias(TinhThanh[i].name),
    }));
    this.state = {isSearch: '', keySearch: ''};
  }

  change_alias(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');
    str = str.replace(
      /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
      ' ',
    );
    str = str.replace(/ + /g, ' ');
    str = str.trim();
    return str;
  }

  render() {
    const {navigation} = this.props;
    const {keySearch} = this.state;
    var dataNN = null;
    if (keySearch.length > 0) {
      dataNN = this.listPlace.filter(
        (item) =>
          (item = item.nameSearch.indexOf(this.change_alias(keySearch)) > -1),
      );
    } else {
      dataNN = this.listPlace;
    }
    // console.log(listPlace);
    return (
      <View style={styles.content}>
        <Header title="Chọn địa điểm" navigation={navigation} back />
        <View style={styles.viewSearch}>
          <TextInput
            icon={<IconSearch width={24} height={24} />}
            placeholder="Tìm kiếm"
            style={{flex: 1}}
            onChangeText={(value) => this.setState({keySearch: value})}
            iconRight={
              <TouchableOpacity>
                <IconX width={24} heigh={24} />
              </TouchableOpacity>
            }
          />
        </View>
        <View style={styles.padding10}>
          <TouchableOpacity>
            <TextView h4>Tự động xác định vị trí</TextView>
          </TouchableOpacity>
        </View>

        <FlatList
          ListHeaderComponent={
            <TextView
              h7
              style={{
                color: COLOR_BG_DARK_GREY,
              }}>
              Tỉnh/ thành phố
            </TextView>
          }
          style={styles.flatlist}
          contentContainerStyle={styles.contentFlatlist}
          data={dataNN}
          renderItem={({item}) => (
            <TouchableOpacity style={styles.itemPlace}>
              <TextView h5 style={styles.txtPlace}>
                {item.name}
              </TextView>
              <IconRight />
            </TouchableOpacity>
          )}
          keyExtractor={(item, index) => `${index}`}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  content: {backgroundColor: COLOR_GRAY, flex: 1},
  padding10: {
    padding: 20,
    backgroundColor: COLOR_BG_WHITE,
    marginBottom: 5,
  },
  viewSearch: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    alignItems: 'center',
    paddingVertical: 10,
  },
  contentFlatlist: {
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  flatlist: {flex: 1, backgroundColor: COLOR_BG_WHITE},
  itemPlace: {
    paddingVertical: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtPlace: {color: '#000B1A', flex: 1},
});
