import React, {Component} from 'react';
import {connect} from 'react-redux';
import {RootNavigation} from '~/navigations/AppNavigation';
import {
  Platform,
  KeyboardAvoidingView,
  View,
  StatusBar,
  BackHandler,
  DeviceEventEmitter,
  RefreshControl,
} from 'react-native';
import {SCLAlert, SCLAlertButton} from 'react-native-scl-alert';
import {BallIndicator} from 'react-native-indicators';
import {NavigationBack, hideModal} from '~/redux/actions/anotherActions';
import {isLoading, Modal, UserState, LangState} from '~/redux/selectors';
import {CT_LOCALE} from '..';
import {setLocale} from '~/Localizations/i18n';

class Screens extends Component {
  constructor(props) {
    super(props);
    this.state = {
      langST: '',
    };
  }
  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener(
        'hardwareBackPress',
        this._handleHardwareBackPress,
      );
    }

    DeviceEventEmitter.addListener(CT_LOCALE, () => {
      this.forceUpdate();
    });
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        this._handleHardwareBackPress,
      );
    }
  }
  _handleHardwareBackPress = () => {
    const {NavigationBack, nav} = this.props;
    if (nav.index === 0) {
      BackHandler.exitApp();
      return false;
    }
    NavigationBack();
    return true;
  };
  modalTitle = (type) => {
    if (type === 'warning') {
      return 'Cảnh Báo';
    }
    if (type === 'danger') {
      return 'Lỗi';
    }
    if (type === 'success') {
      return 'Thành Công';
    }
    if (type === 'info') {
      return 'Thông tin';
    }
    return '';
  };

  renderView() {
    const {isLoading, modalState, hideModal,LangState} = this.props;
    setLocale(LangState);
    console.log('re-render');
    return (
      <View style={{flex: 1}}>
        <RootNavigation/>
        <SCLAlert
          theme={modalState.typez}
          show={modalState.show}
          title={this.modalTitle(modalState.typez)}
          subtitle={modalState.title}
          onRequestClose={() => hideModal()}>
          <SCLAlertButton theme={modalState.typez} onPress={() => hideModal()}>
            Đóng
          </SCLAlertButton>
        </SCLAlert>
        {isLoading && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              backgroundColor: 'rgba(0, 0, 0, 0.3)',
              zIndex: 2,
            }}>
            <BallIndicator />
          </View>
        )}
      </View>
    );
  }

  render() {
    if (Platform.OS === 'ios') {
      return (
        <KeyboardAvoidingView style={{flex: 1}} behavior="padding">
          <StatusBar barStyle="light-content" translucent />
          {this.renderView()}
        </KeyboardAvoidingView>
      );
    }
    return <View style={{flex: 1}}>{this.renderView()}</View>;
  }
}
const mapStateToProps = (state) => ({
  nav: state.nav,
  isLoading: isLoading(state),
  modalState: Modal(state),
  UserState: UserState(state),
  LangState: LangState(state),
});

export default connect(mapStateToProps, {NavigationBack, hideModal})(Screens);
