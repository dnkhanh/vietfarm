import React, {Component} from 'react';
import {View, ScrollView, TouchableOpacity, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {IconX, IconEye, IconUserName, IconLock} from '~/image';
import {
  COLOR_BG_WHITE,
  PADDING_HEADER,
  COLOR_BG_DARK_GREY,
  IS_PHONE_X,
  COLOR_BG_DARK_GREY_LIGHT,
  SIZE_H3,
  TYPE_MODAL_DANGER,
  TYPE_MODAL_WARNING,
} from '~/AppValues';
import {regexEmail, regexPhone, regexPassword} from '~/helpers/validates';
import {TextView, InputLine, Button} from '~/components';
import {loginSubmit, isLogin} from '~/redux/actions/auth';
import {NavigationRouter, showModal} from '~/redux/actions/anotherActions';
import {getString, setLocale} from '~/Localizations/i18n';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secureTextEntry: true,
      Username: null,
      Pass: null,
      alertText: null,
      alertType: 'danger',
    };
  }
  onSubmit() {
    const {loginSubmit, showModal} = this.props;
    const {Username, Pass} = this.state;
    // const checkSubmit = this.checkSubmit();
    if (true) {
      loginSubmit(Username, Pass);
    } else {
      // console.log('API Đăng nhập');
      // showModal(checkSubmit, TYPE_MODAL_WARNING);
    }
  }

  // checkSubmit() {
  //   const {Username, Pass} = this.state;
  //   if (!Username || !Pass) {
  //     return getString('login.error');
  //   }
  //   const validateEmail = regexEmail(Username);
  //   const validatePhone = regexPhone(Username);
  //   if (validateEmail && validatePhone) {
  //     const abc = firebase()
  //     .httpsCallable('addProduct')({
  //       title: 'title-'+Math.random(),
  //       image: 'image-'+Math.random(),
  //       price: Math.random(),
  //     })
  //     console.log(abc);
  //     // const aaa = firestore()
  //     //   .collection('users')
  //     //   .get()
  //     //   .then(querySnapshot => {
  //     //     console.log('Total users: ', querySnapshot.size);

  //     //     querySnapshot.forEach(documentSnapshot => {
  //     //       console.log('User ID: ', documentSnapshot.id, documentSnapshot.data());
  //     //     });
  //     //   });
  //     return getString('login.error_username_1');
  //   }
  //   const validatePass = regexPassword(Pass);
  //   if (validatePass) {
  //     return validatePass;
  //   }
  //   return false;
  // }

  render() {
    const {navigation, isLogin} = this.props;
    const {secureTextEntry, Username, Pass} = this.state;
    const infoUser = {}
    isLogin(infoUser);
    return (
      <View style={{flex: 1, backgroundColor: COLOR_BG_WHITE}}>
        <ScrollView
          style={{
            ...PADDING_HEADER,
            padding: 15,
          }}>
          <TouchableOpacity
            style={{alignItems: 'flex-end'}}
            onPress={() => navigation.goBack()}>
            <IconX width={34} height={34} />
          </TouchableOpacity>

          <View style={styles.title}>
            <TextView h0>Đăng nhập</TextView>
          </View>
          <View style={{paddingHorizontal: 15, paddingVertical: 15}}>
            <InputLine
              icon={<IconUserName />}
              styleIcon={styles.paddingRight10}
              value={Username}
              placeholder={getString('login.username')}
              placeholderTextColor={COLOR_BG_DARK_GREY}
              onChangeText={(value) => this.setState({Username: value})}
            />
            <InputLine
              icon={<IconLock />}
              styleIcon={styles.paddingRight10}
              iconRight={
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      secureTextEntry: secureTextEntry ? false : true,
                    })
                  }>
                  <IconEye />
                </TouchableOpacity>
              }
              value={Pass}
              onChangeText={(value) => this.setState({Pass: value})}
              secureTextEntry={secureTextEntry}
              placeholder={getString('login.pass')}
              placeholderTextColor={COLOR_BG_DARK_GREY}
            />
            <View style={{paddingTop: 20}}>
              <Button
                styleText={{fontSize: SIZE_H3}}
                onPress={() => {
                  this.onSubmit();
                }}
                title={getString('login.submit')}
              />
            </View>
          </View>
          <View style={styles.Text_Bottom}>
            <TextView style={styles.text_1}>
              {getString('login.forgotpass')}
            </TextView>
            <View style={{flexDirection: 'row'}}>
              <TextView style={styles.text_2}>
                {getString('login.nothaveacc')}
              </TextView>
              <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                <TextView style={{color: '#0091FF'}}>
                  {getString('login.regnow')}
                </TextView>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default connect(null, {
  NavigationRouter,
  showModal,
  loginSubmit,
  isLogin,
})(Login);
const styles = StyleSheet.create({
  paddingRight10: {
    paddingRight: 10,
  },
  container: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
  title: {
    alignItems: 'center',
    paddingVertical: 40,
  },
  Text_Bottom: {
    alignItems: 'center',
    paddingBottom: IS_PHONE_X ? 25 : 2,
  },
  text_1: {
    color: COLOR_BG_DARK_GREY_LIGHT,
    paddingBottom: 10,
  },
  text_2: {
    color: COLOR_BG_DARK_GREY_LIGHT,
    paddingRight: 10,
  },
});
