import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  FlatList,
  Dimensions,
  TouchableOpacity,
  DeviceEventEmitter,
} from 'react-native';
import {connect} from 'react-redux';
import {
  COLOR_BG_APP,
  COLOR_GRAY,
  COLOR_BG_WHITE,
  PADDING_HEADER,
  SHADOW_STYLE,
} from '~/AppValues';
const {width, height} = Dimensions.get('window');
import {
  Dacsan,
  Thit,
  Rau,
  IconSearch,
  IconBell,
  IconX,
  IconLeft,
} from '~/image';
import {TextInput, TextView, ItemProduct} from '~/components';
import Search from '../Search';
import {getString} from '~/Localizations/i18n';
import {dataProduct} from '~/dataTemp';
import {CT_LOCALE} from '~/';
import firebase from '@react-native-firebase/functions';
import auth from '@react-native-firebase/auth';
// const data = firebase().httpsCallable('addProduct')({
//   title: 'Thịt bò',
//   image: 'http://matongphonghuong.com/Uploaded/Members/12166/images/2018/kien-thuc-ve-tam-that/thit-bo.jpg',
//   price: 30000,
//   uid: infoUser.uid,
// });
// if (data) {
//   console.log('Thêm thành công');
// }

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSearch: false,
      dataProduct: '',
    };
  }
  UNSAFE_componentWillMount() {
    firebase()
      .httpsCallable('listProduct')({limit: 10})
      .then((result) => {
        this.setState({dataProduct: result.data});
      })
      .catch((error) => {
        console.log(error);
      });
  }
  itemHomeTop(name, icon) {
    return (
      <View style={styles.cate_home_item}>
        <View style={styles.tinyLogo}>{icon}</View>
        <TextView>{name}</TextView>
      </View>
    );
  }
  render() {
    const {isSearch} = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.content}>
        <View style={styles.viewSearch}>
          {isSearch && (
            <TouchableOpacity
              onPress={() => this.setState({isSearch: false})}
              style={{paddingRight: 5}}>
              <IconLeft width={24} height={24} />
            </TouchableOpacity>
          )}

          <TextInput
            icon={<IconSearch width={24} height={24} />}
            placeholder={getString('home.textInput')}
            style={{marginRight: 10, flex: 1}}
            onFocus={() => this.setState({isSearch: true})}
            onBlur={() => this.setState({isSearch: false})}
            iconRight={
              <TouchableOpacity onPress={() => console.warn('click')}>
                <IconX width={24} heigh={24} />
              </TouchableOpacity>
            }
          />
          {!isSearch && (
            <TouchableOpacity
              onPress={() => navigation.navigate('Notification')}>
              <IconBell width={24} height={24} />
            </TouchableOpacity>
          )}
        </View>
        {isSearch ? (
          <Search />
        ) : (
          <FlatList
            style={{flex: 1, padding: 10}}
            ListHeaderComponent={() => (
              <>
                <View>
                  <TextView h1 type="bold">
                    {getString('home.title')}
                  </TextView>
                </View>
                <View style={styles.cate_home}>
                  {this.itemHomeTop(
                    getString('home.rau'),
                    <Rau width={48} height={48} />,
                  )}
                  {this.itemHomeTop(
                    getString('home.thit'),
                    <Thit width={48} height={48} />,
                  )}
                  {this.itemHomeTop(
                    getString('home.dacsan'),
                    <Dacsan width={48} height={48} />,
                  )}
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('Chat')}>
                  <Image
                    resizeMode="cover"
                    style={{
                      width: width - 20,
                      height: (159 * (width - 20)) / 328,
                      borderRadius: 5,
                    }}
                    source={require('~/image/Product/banner.jpg')}
                  />
                </TouchableOpacity>
                <TextView h5 type="bold" style={{paddingVertical: 10}}>
                  {getString('home.productTop')}
                </TextView>
              </>
            )}
            data={dataProduct}
            numColumns={2}
            renderItem={({item}) => (
              <ItemProduct
                image={item.image}
                title={item.title}
                price={item.price}
                onPress={() => {
                  navigation.navigate('ProductDetail', {
                    item,
                  });
                }}
              />
            )}
            keyExtractor={(item, index) => `${index}`}
          />
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  viewSearch: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    paddingBottom: 10,
    backgroundColor: COLOR_BG_WHITE,
    ...PADDING_HEADER,
    ...SHADOW_STYLE,
  },
  cate_home_item: {
    flex: 0.33,
    alignContent: 'center',
    alignItems: 'center',
  },
  cate_home: {
    flexDirection: 'row',
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderColor: COLOR_GRAY,
    marginBottom: 20,
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_BG_APP,
  },
  tinyLogo: {
    borderRadius: 16,
    borderColor: '#D8DADD',
    borderWidth: 1,
    padding: 10,
    marginBottom: 10,
  },
});

export default connect(null, {})(Home);
