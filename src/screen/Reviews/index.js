import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {Header, Button, TextView, Rating} from '~/components';
import {
  WIDTH_DEVICE,
  COLOR_BG_WHITE,
  COLOR_BG_GREY,
  COLOR_BG_DARK_GREY,
  COLOR_BG_BLACK,
  COLOR_BORDER_TEXT_INPUT,
  SIZE_H4,
} from '~/AppValues';
import {IconCamera} from '~/image';
import ImagePicker from 'react-native-image-picker';

// options image picker
const options = {
  title: 'Tải ảnh lên',
  cancelButtonTitle: 'Huỷ',
  takePhotoButtonTitle: 'Chụp ảnh',
  chooseFromLibraryButtonTitle: 'Chọn từ thư viện',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
// data item
const data = {
  image:
    'https://cdn-www.vinid.net/2019/09/Nen-chon-loai-hoa-qua-bieu-Tet-2019-nao.jpg',
  title: 'Xà lách đà lạt',
};
export default class Reviews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgPicker: false,
      value_comment: '',
      star: 0,
      avatarSource: false,
    };
  }
  render() {
    const {navigation} = this.props;
    const {imgPicker, value_comment, star, avatarSource} = this.state;
    var max_value_comment = 4000;
    let count_value_comment = value_comment.length;
    return (
      <View style={{flex: 1, backgroundColor: COLOR_BG_WHITE}}>
        <Header title="Nhận xét" navigation={navigation} exit />
        <ScrollView style={{padding: 10}}>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 0.35, paddingRight: 10}}>
              <Image source={{uri: data.image}} style={styles.img} />
            </View>
            <View style={{flex: 0.65, paddingRight: 10}}>
              <TextView h4 type="bold">
                {data.title}
              </TextView>
            </View>
          </View>
          <View style={styles.viewRate}>
            <TextView h5>Đánh giá của bạn</TextView>
            <Rating
              starSize={40}
              disabled={false}
              rating={star}
              selectedStar={(rating) => this.setState({star: rating})}
            />
          </View>
          {!avatarSource ? (
            <TouchableOpacity
              style={styles.imagePicker}
              onPress={() => {
                ImagePicker.showImagePicker(options, (response) => {
                  console.log('Response = ', response);

                  if (response.didCancel) {
                    console.log('User cancelled image picker');
                  } else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                  } else if (response.customButton) {
                    console.log(
                      'User tapped custom button: ',
                      response.customButton,
                    );
                  } else {
                    const source = {uri: response.uri};
                    this.setState({
                      avatarSource: source,
                      imgPicker: false,
                    });
                  }
                });
              }}>
              <IconCamera />
              <TextView h5 style={{color: COLOR_BG_DARK_GREY, paddingTop: 20}}>
                Thêm hình ảnh
              </TextView>
            </TouchableOpacity>
          ) : (
            <View style={styles.imagePicker}>
              <Image
                style={{width: 200, height: 200}}
                source={{uri: avatarSource.uri}}
              />
            </View>
          )}
          <View>
            <TextInput
              style={styles.textInput}
              placeholder="Thêm nội dung đánh giá"
              multiline
              placeholderTextColor={COLOR_BG_BLACK}
              onChangeText={(value) => {
                count_value_comment <= max_value_comment &&
                  this.setState({value_comment: value});
              }}
            />
            <TextView style={{flex: 1, paddingTop: 5, textAlign: 'right'}}>
              {count_value_comment}/{max_value_comment}
            </TextView>
          </View>
        </ScrollView>
        <Button simple title="Thêm đánh giá" />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  imagePicker: {
    flex: 1,
    paddingVertical: 50,
    backgroundColor: COLOR_BG_GREY,
    alignItems: 'center',
  },
  img: {
    height: WIDTH_DEVICE / 4.5,
    width: '100%',
    borderRadius: 10,
  },
  viewRate: {
    paddingTop: 15,
    paddingBottom: 25,
    alignContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    marginTop: 20,
    borderColor: COLOR_BORDER_TEXT_INPUT,
    borderBottomWidth: 1,
    paddingVertical: 10,
    flex: 1,
    fontSize: SIZE_H4,
  },
});
