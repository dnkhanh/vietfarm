import React, {Component} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {COLOR_BG_WHITE, COLOR_BG_DARK_GREY, COLOR_GRAY} from '../../AppValues';
import {TextView, ActionSheet} from '~/components';
import {IconDot3, IconTick} from '../../image';
import Header from '../../components/Header';
import {getString} from '../../Localizations/i18n';

class Notification extends Component {
  constructor(props) {
    super(props);
    this.defaultTitles = [
      {
        title: 'Đánh dầu đã đọc tất cả',
        action: () => {},
      },
      {
        title: 'Xóa tất cả',
        actionStyle: 'destructive',
        action: () => {},
      },
      {
        title: 'Hủy',
        actionStyle: 'cancel',
      },
    ];
    this.state = {};
  }

  viewOrder(index) {
    return (
      <View
        style={{
          paddingVertical: 10,
          backgroundColor: index % 2 === 0 ? COLOR_GRAY : COLOR_BG_WHITE,
        }}>
        <View style={[styles.flexDirection, {paddingHorizontal: 10}]}>
          <IconTick width={30} height={30} />
          <View style={{flex: 1, paddingHorizontal: 10}}>
            <TextView numberOfLines={2} h5 style={{color: '#000000'}}>
              Thông báo tình trạng đơn hàng
            </TextView>
            <TextView h7 style={{color: COLOR_BG_DARK_GREY}}>
              29/02/2020
            </TextView>
          </View>
          <IconDot3 width={24} height={24} />
        </View>
        <TextView h6 style={{paddingHorizontal: 10, color: COLOR_BG_DARK_GREY}}>
          Đơn hàng #669226132 (Xà lách Đà Lạt) của quý khách đã đặt thành công
          chúng tôi sẽ giao hàng cho quý khách vào ngày 29/11/2018
        </TextView>
      </View>
    );
  }
  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.content}>
        {/* View header */}
        <Header
          title={getString('notification.header')}
          navigation={navigation}
          back
          option={() =>
            this.setState({titles: this.defaultTitles}, () => {
              this.refs.picker.show();
            })
          }
        />
        {/* View item don hang */}
        <ScrollView>
          {this.viewOrder(0)}
          {this.viewOrder(1)}
          {this.viewOrder(2)}
          {this.viewOrder(3)}
          {this.viewOrder(0)}
          {this.viewOrder(1)}
          {this.viewOrder(2)}
        </ScrollView>

        <ActionSheet
          ref="picker"
          titles={this.state.titles}
          separateHeight={1}
          separateColor="#dddddd"
          backgroundColor="rgba(0, 0, 0, 0.5)"
          containerStyle={{margin: 15, borderRadius: 5}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flexDirection: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
});

export default Notification;
