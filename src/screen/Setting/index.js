import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {COLOR_BG_WHITE, COLOR_BG_DARK_GREY, COLOR_GRAY} from '~/AppValues';
import {
  IconEdit,
  IconShop,
  IconRight,
  IconPeople,
  IconFileText,
  IconLove,
  IconMessageCircle,
  IconLogout,
} from '~/image';
import {Header, TextView} from '~/components';
import {LangState} from '~/redux/selectors';
import {getString, setLocale} from '~/Localizations/i18n';
import {CT_LOCALE} from '../..';
import {connect} from 'react-redux';
import {changeLanguage} from '~/redux/actions/anotherActions';
import auth from '@react-native-firebase/auth';
import {userlogOut} from '~/redux/actions/auth';
class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: 'vi',
    };
  }

  viewItem(title, icon, onPress) {
    return (
      <TouchableOpacity
        style={styles.viewItem}
        onPress={() => {
          if (onPress) {
            onPress();
          }
        }}>
        <View style={styles.viewIcon}>{icon}</View>
        <TextView h5 style={{flex: 1, padding: 10}}>
          {title}
        </TextView>
        <IconRight style={{alignSelf: 'center', paddingHorizontal: 10}} />
      </TouchableOpacity>
    );
  }
  render() {
    const {navigation, userlogOut, changeLanguage, LangState} = this.props;

    const infoUser = {};
    return (
      <View style={styles.content}>
        <Header title={getString('setting.header')} option />
        {/* View profile */}
        <View style={styles.viewName}>
          <Image
            source={require('~/image/ngoctrinh.jpg')}
            style={{width: 50, height: 50, borderRadius: 25}}
          />
          <View
            style={{
              flex: 1,
              paddingHorizontal: 10,
              justifyContent: 'center',
            }}>
            <TextView h5 type="bold">
              Mrs. Ngọc Trinh
            </TextView>
            <TextView h7 style={{color: COLOR_BG_DARK_GREY, paddingTop: 5}}>
              {infoUser.email}
            </TextView>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('Edit')}
            style={{alignSelf: 'center'}}>
            <IconEdit width={25} height={25} />
          </TouchableOpacity>
        </View>
        <View style={{backgroundColor: COLOR_GRAY, height: 8}} />
        <ScrollView style={{flex: 1}}>
          {this.viewItem(
            getString('setting.your_store'),
            <IconShop height={18} width={18} />,
            () => navigation.navigate('ListMyStore'),
          )}
          {this.viewItem(
            getString('setting.mng_acc'),
            <IconPeople height={18} width={18} />,
          )}
          {this.viewItem(
            getString('setting.mng_orders'),
            <IconFileText height={18} width={18} />,
          )}
          {this.viewItem(
            getString('setting.fav_products'),
            <IconLove height={18} width={18} />,
          )}
          {this.viewItem(
            getString('setting.your_rate'),
            <IconMessageCircle height={18} width={18} />,
          )}

          <View style={{backgroundColor: COLOR_GRAY, height: 8}} />
          {this.viewItem(
            getString('setting.info_address'),
            <IconMessageCircle height={18} width={18} />,
          )}
          {this.viewItem(
            getString('setting.info_payments'),
            <IconMessageCircle height={18} width={18} />,
          )}
          {this.viewItem(
            'Đăng xuất',
            <IconLogout height={18} width={18} />,
            () => userlogOut('redirect'),
          )}
          {this.viewItem(
            getString('setting.change_language'),
            <IconMessageCircle height={18} width={18} />,
            () => {
              if (LangState == 'en') {
                changeLanguage('vi');
              }
              if (LangState == 'vi') {
                changeLanguage('en');
              }
            },
          )}
          <View style={{backgroundColor: COLOR_GRAY, height: 8}} />
          <View style={{padding: 10}}>
            <TextView
              h7
              style={{color: COLOR_BG_DARK_GREY, paddingVertical: 10}}>
              {getString('setting.text_support')}
            </TextView>
            <TextView h6 style={{paddingBottom: 10}}>
              {getString('setting.text_contact')}
            </TextView>
            <TextView h6>{getString('setting.text_report')}</TextView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flexDirection: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
  viewName: {
    flexDirection: 'row',
    padding: 10,
  },
  viewItem: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  viewIcon: {
    height: 40,
    width: 40,
    borderRadius: 20,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLOR_GRAY,
  },
});
const mapStateToProps = (state) => ({
  LangState: LangState(state),
});
export default connect(mapStateToProps, {userlogOut, changeLanguage})(Setting);
