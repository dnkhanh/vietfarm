import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
} from 'react-native';
import {
  COLOR_BG_WHITE,
  COLOR_GRAY,
  PADDING_HEADER,
  SHADOW_STYLE,
} from '~/AppValues';
import {TextView, Header, InputLine, Button} from '~/components';
import {getString} from '~/Localizations/i18n';
export default class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: 'DnKhanh',
      Email: 'DnKhanh@gmail.com',
      phoneNumber: '0973563159',
      passOld: 'ssssss',
      passNew: 'ssssss',
    };
  }
  render() {
    const {navigation} = this.props;
    const {fullName,Email,phoneNumber,passOld,passNew} = this.state;
    return (
      <View style={styles.content}>
        <Header
          title={getString('edit_profile.header')}
          navigation={navigation}
          back
        />

        <ScrollView style={{flex: 1}}>
          <View style={{padding: 10}}>
            <InputLine
              title={getString('edit_profile.text_fullname')}
              value={fullName}
              onChangeText={(value) => this.setState({fullName:value})}
            />
            <InputLine
              title={getString('edit_profile.text_email')}
              value={Email}
              onChangeText={(value) => this.setState({Email:value})}
            />
            <InputLine
              title={getString('edit_profile.text_phone')}
              value={phoneNumber}
              onChangeText={(value) => this.setState({phoneNumber:value})}
            />
          </View>
          <View style={{backgroundColor: COLOR_GRAY, height: 8}} />
          <View style={{padding: 10}}>
            <TextView h5 style={{paddingVertical: 20}}>
              {getString('edit_profile.title_change')}
            </TextView>
            <InputLine
              secureTextEntry
              title={getString('edit_profile.text_passold')}
              value={passOld}
              onChangeText={(value) => this.setState({passOld:value})}
            />
            <InputLine
              secureTextEntry
              title={getString('edit_profile.text_passnew')}
              value={passNew}
              onChangeText={(value) => this.setState({passNew:value})}
            />
          </View>
        </ScrollView>
        <Button simple title={getString('edit_profile.btn_update')} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  flexDirection: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
  viewHeader: {
    backgroundColor: COLOR_BG_WHITE,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
    ...PADDING_HEADER,
    ...SHADOW_STYLE,
  },
  viewName: {
    flexDirection: 'row',
    padding: 10,
  },
  viewItem: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  viewIcon: {
    height: 40,
    width: 40,
    borderRadius: 20,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLOR_GRAY,
  },
});
