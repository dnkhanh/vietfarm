import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {
  COLOR_BG_APP,
  COLOR_BG_WHITE,
  PADDING_HEADER,
  SHADOW_STYLE,
  COLOR_BG_DARK_GREY,
  COLOR_BG_GREY,
} from '~/AppValues';
import {getString} from '~/Localizations/i18n';

import {
  ItemProduct,
  Button,
  TextInput,
  TextView,
  ActionSheet,
} from '~/components';
import {IconSearch, IconBell, IconLeft, IconX, IconDown} from '~/image';
import Search from '../Search';
import {dataProduct} from '~/dataTemp';
const listBtn = ['Mới nhất', 'Giảm giá', 'Bán chạy nhất', 'Top sale'];

export default class Suggest extends Component {
  constructor(props) {
    super(props);
    this.defaultTitles = [
      {
        title: getString('suggest.hasVerify'),
        action: () => {},
      },
      {
        title: getString('suggest.noVerify'),
        action: () => {},
      },
      {
        title: 'Hủy',
        actionStyle: 'cancel',
      },
    ];
    this.state = {
      titles: this.defaultTitles,
      selectFilter: 0,
      isSearch: false,
    };
  }
  render() {
    const {isSearch} = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.title_wel}>
        <View style={styles.viewSearch}>
          {isSearch && (
            <TouchableOpacity
              onPress={() => this.setState({isSearch: false})}
              style={{paddingRight: 5}}>
              <IconLeft width={24} height={24} />
            </TouchableOpacity>
          )}

          <TextInput
            icon={<IconSearch width={24} height={24} />}
            placeholder={getString('suggest.textInput')}
            style={{marginRight: 10, flex: 1}}
            onFocus={() => this.setState({isSearch: true})}
            onBlur={() => this.setState({isSearch: false})}
            iconRight={
              <TouchableOpacity onPress={() => console.warn('click')}>
                <IconX width={24} heigh={24} />
              </TouchableOpacity>
            }
          />
          {!isSearch && (
            <TouchableOpacity
              onPress={() => navigation.navigate('Notification')}>
              <IconBell width={24} height={24} />
            </TouchableOpacity>
          )}
        </View>
        {isSearch ? (
          <Search />
        ) : (
          <>
            <View style={{backgroundColor: '#F5F6F8'}}>
              <ScrollView
                contentContainerStyle={{
                  padding: 10,
                }}
                horizontal
                showsHorizontalScrollIndicator={false}>
                {listBtn.map((i, index) => (
                  <Button
                    style={{marginRight: 10}}
                    light={index !== this.state.selectFilter}
                    key={i}
                    title={i}
                    onPress={() => this.setState({selectFilter: index})}
                  />
                ))}
              </ScrollView>
            </View>
            <View style={styles.viewFilter}>
              <TouchableOpacity
                style={styles.itemFilter}
                onPress={() => {
                  navigation.navigate('SearchPlace');
                }}>
                <TextView h5 style={styles.txtFilter}>
                 {getString('suggest.place')}
                </TextView>
                <IconDown />
              </TouchableOpacity>
              <View style={{width: 1, backgroundColor: COLOR_BG_GREY, marginRight:10}} />
              <TouchableOpacity
                style={styles.itemFilter}
                onPress={() =>
                  this.setState({titles: this.defaultTitles}, () => {
                    this.refs.picker.show();
                  })
                }>
                <TextView h5 style={styles.txtFilter}>
                  {getString('suggest.reputation')}
                </TextView>
                <IconDown />
              </TouchableOpacity>
            </View>
            <FlatList
              style={{padding: 10}}
              data={dataProduct}
              numColumns={2}
              renderItem={({item}) => (
                <ItemProduct
                  image={item.image}
                  title={item.title}
                  price={item.price}
                  onPress={() => console.log('click', item)}
                />
              )}
              keyExtractor={(item, index) => `${index}`}
            />
          </>
        )}
        <ActionSheet
          ref="picker"
          titles={this.state.titles}
          separateHeight={1}
          separateColor="#dddddd"
          backgroundColor="rgba(0, 0, 0, 0.5)"
          containerStyle={{margin: 15, borderRadius: 5}}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  viewSearch: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    paddingBottom: 10,
    backgroundColor: COLOR_BG_WHITE,
    ...PADDING_HEADER,
    ...SHADOW_STYLE,
  },
  title_wel: {
    flex: 1,
    backgroundColor: COLOR_BG_APP,
  },
  viewFilter: {
    flexDirection: 'row',
    borderBottomColor: COLOR_BG_GREY,
    borderBottomWidth: 1,
    padding: 10,
  },
  itemFilter: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  txtFilter: {color: COLOR_BG_DARK_GREY, paddingRight: 10},
});
