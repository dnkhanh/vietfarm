import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {Header, TextView, Rating, Button} from '~/components';
import {
  WIDTH_DEVICE,
  COLOR_BG_RED,
  COLOR_BG_GREEN,
  COLOR_BG_GREEN_LIGHT,
  COLOR_BG_WHITE,
  COLOR_BG_BLACK,
} from '~/AppValues';
import {formatNumber} from '~/helpers/format';
import {IconLove, IconVerify, IconLocation} from '~/image';
import {DATA_COMMENT} from '~/dataTemp';
import {getString} from '~/Localizations/i18n';
import { connect } from 'react-redux';
class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {navigation} = this.props;
    const {id, title, img, price} = navigation.state.params.item;
    const data_item = {id, title, img, price};
    return (
      <View style={{flex: 1}}>
        <Header title="" navigation={navigation} back share option />
        <ScrollView style={styles.contain}>
          <View style={{width: '100%', height: WIDTH_DEVICE - 150}}>
            <Image
              style={{width: '100%', height: '100%'}}
              source={require('~/image/img_post_1.png')}
            />
          </View>
          <View style={{flex: 1, paddingHorizontal: 10}}>
            <View style={[styles.paddingVe, {flexDirection: 'row'}]}>
              <TextView h3 type="bold" style={{flex: 1}}>
                {title}
              </TextView>
              <IconLove width={30} height={30} />
            </View>
            <TextView h6 type="bold" style={{color: COLOR_BG_RED}}>
              {formatNumber(price)}
            </TextView>
            <View style={[styles.paddingVe, {flexDirection: 'row'}]}>
              <Rating rating={5} starSize={20} />
              <TextView type="bold" style={{paddingHorizontal: 7}} color="#000">
                4,6
              </TextView>
              <TextView>(56 {getString('product_detail.text_rate')})</TextView>
            </View>
            <View style={styles.viewVerify}>
              <View style={{marginTop: 5}}>
                <IconVerify />
              </View>
              <View style={{flex: 1, paddingLeft: 10}}>
                <TextView h5 style={{color: COLOR_BG_GREEN}}>
                  Đã update các giấy tờ liên quan doanh nghiệp
                </TextView>
                <TextView h6 style={{paddingTop: 10}}>
                  - Giấy chứng nhận đăng ký kinh doanh ngành nghề kinh doanh
                  thực phẩm
                </TextView>
                <TextView h6 style={{paddingTop: 10}}>
                  - Đăng ký xác nhận kiến thức An toàn thực phẩm
                </TextView>
                <TextView h6 style={{paddingTop: 10}}>
                  - Đăng ký xác nhận kiến thức An toàn thực phẩm
                </TextView>
              </View>
            </View>
            <View style={styles.ViewInfo}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Image
                  source={require('~/image/img_post_1.png')}
                  style={styles.avatar}
                />
                <View style={styles.viewName}>
                  <TextView h5 style={{color: COLOR_BG_BLACK}}>
                    Trai rau sach GoDream
                  </TextView>
                  <View style={styles.viewLocation}>
                    <IconLocation height={15} width={15} />
                    <TextView
                      h7
                      style={{color: COLOR_BG_BLACK, paddingHorizontal: 10}}>
                      Vinh
                    </TextView>
                  </View>
                </View>
              </View>
              <TouchableOpacity style={styles.buttonGo}>
                <TextView h7 style={{color: COLOR_BG_RED}}>
                  {getString('product_detail.text_go_store')}
                </TextView>
              </TouchableOpacity>
            </View>
            <View>
              <TextView h5 type="bold" style={{paddingBottom: 10}}>
                {getString('product_detail.text_detail_product')}
              </TextView>
              <TextView h6 style={{color: '#7B7B81'}}>
                Xà lách mỡ chứa nhiều chất xơ có lợi cho tiêu hóa, giàu giá trị
                dinh dưỡng, chứa nhiều vitamin và khoáng chất, có công dụng bồi
                bổ sức khỏe, chống oxy hóa, ngăn ngừa ung thư... Xà lách mỡ có
                thể dùng để chế biến nhiều món ăn ngon như luộc, canh, xào,
                salad... Bạn có thể dễ dàng bảo quản sản phẩm nơi khô ráo hoặc
                ngăn mát tủ lạnh.
              </TextView>
            </View>
            <View>
              <TextView h5 type="bold" style={styles.paddingVe}>
                {getString('product_detail.text_rate_product')}
              </TextView>
              <View style={{flexDirection: 'row'}}>
                <Rating rating={5} starSize={20} />
                <TextView
                  type="bold"
                  style={{paddingHorizontal: 7}}
                  color="#000">
                  4,6
                </TextView>
                <TextView>
                  (56 {getString('product_detail.text_rate')})
                </TextView>
              </View>
              {DATA_COMMENT.map((item, index) => (
                <View style={styles.userComment} key={index}>
                  <Image
                    style={styles.userComment_Avatar}
                    source={item.avatar}
                  />
                  <View style={{flex: 1}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <View style={{flex: 1}}>
                        <TextView>{item.fullName}</TextView>
                        <View style={{alignItems: 'flex-start'}}>
                          <Rating rating={3} starSize={20} />
                        </View>
                      </View>
                      <TextView>26/12/2018</TextView>
                    </View>
                    <View style={{flex: 1, paddingTop: 10}}>
                      <TextView numberOfLines={3}>{item.textComment}</TextView>
                    </View>
                  </View>
                </View>
              ))}
              <TouchableOpacity
                style={{alignContent: 'center', alignItems: 'center'}}>
                <TextView h5 style={{color: COLOR_BG_RED, paddingBottom: 10}}>
                  {getString('product_detail.text_more_rate_comment')}
                </TextView>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <Button
          simple
          title={getString('product_detail.text_order')}
          onPress={() => navigation.navigate('Order', data_item)}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  userComment: {
    flexDirection: 'row',
    paddingVertical: 10,
  },
  userComment_Avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 10,
  },
  paddingVe: {
    paddingVertical: 10,
  },
  ViewInfo: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',

    paddingVertical: 30,
  },
  buttonGo: {
    borderWidth: 1,
    borderColor: 'red',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 6,
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#fff',
  },

  viewVerify: {
    flexDirection: 'row',
    padding: 10,
    borderColor: COLOR_BG_GREEN,
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: COLOR_BG_GREEN_LIGHT,
  },
  viewName: {
    paddingHorizontal: 10,
  },
  viewLocation: {
    flexDirection: 'row',
    paddingTop: 5,
  },
  contain: {
    paddingBottom: 0,
    backgroundColor: COLOR_BG_WHITE,
  },
});
export default connect(null,null)(ProductDetail)
