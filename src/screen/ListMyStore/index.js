import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {COLOR_BG_WHITE, COLOR_BG_DARK_GREY} from '~/AppValues';
import {Header, TextView, Button} from '~/components';
import {IconLocation, IconRight} from '~/image';
import {getString} from '~/Localizations/i18n';

class ListMyStore extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  viewItem() {
    const {navigation} = this.props;

    return (
      <TouchableOpacity
        style={styles.viewItem}
        onPress={() => navigation.navigate('MyStoreDetail')}>
        <Image
          source={require('~/image/img_post_1.png')}
          style={{height: 40, width: 40, borderRadius: 20}}
        />
        <View style={styles.viewName}>
          <TextView h5>Trai rau sach GoDream</TextView>
          <View style={styles.viewLocation}>
            <IconLocation height={12} width={12} />
            <TextView
              h7
              style={{color: COLOR_BG_DARK_GREY, paddingHorizontal: 5}}>
              Vinh
            </TextView>
          </View>
        </View>
        <IconRight style={{alignSelf: 'center', paddingHorizontal: 10}} />
      </TouchableOpacity>
    );
  }
  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.content}>
        <Header
          navigation={navigation}
          title={getString('list_my_store.header')}
          back
        />
        <ScrollView style={{flex: 1}}>
          {this.viewItem()}
          {this.viewItem()}
          {this.viewItem()}
        </ScrollView>
        <Button simple title={getString('list_my_store.btn_add')} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
  viewItem: {
    flexDirection: 'row',
    padding: 10,
  },
  viewName: {
    flex: 1,
    paddingHorizontal: 10,
  },
  viewLocation: {
    flexDirection: 'row',
    paddingTop: 5,
    alignItems: 'center',
  },
});
export default ListMyStore;
