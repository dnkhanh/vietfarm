import React, {Component} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {
  COLOR_BG_WHITE,
  COLOR_BG_GREEN,
  COLOR_BG_DARK_GREY,
  COLOR_BG_GREEN_LIGHT,
} from '~/AppValues';
import {
  IconStar,
  IconRight,
  IconMessageCircle,
  IconFileText,
  IconVerify,
  IconShop,
  IconPeople,
} from '~/image';
import {TextView} from '~/components';
const SIZE_ICON = 40;
class Shop extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  viewItem(icon, name, number, note, right) {
    return (
      <View style={styles.viewItem}>
        <View style={styles.viewLeft}>
          {icon}
          <TextView h5>{name}</TextView>
        </View>

        <View style={styles.viewRight}>
          <TextView
            h6
            type="bold"
            style={{color: COLOR_BG_GREEN, paddingRight: 10}}>
            {number}
          </TextView>
          {note && (
            <TextView h6 style={{flex: 1}}>
              ({note})
            </TextView>
          )}
          {right && <IconRight />}
        </View>
      </View>
    );
  }

  viewNote() {
    return (
      <View style={styles.viewVerify}>
        <View style={{marginTop: 5}}>
          <IconVerify />
        </View>
        <View style={{flex: 1, paddingLeft: 10}}>
          <TextView h5 style={{color: COLOR_BG_GREEN}}>
            Đã update các giấy tờ liên quan doanh nghiệp
          </TextView>
          <TextView h6 style={{paddingTop: 10}}>
            - Giấy chứng nhận đăng ký kinh doanh ngành nghề kinh doanh thực phẩm
          </TextView>
          <TextView h6 style={{paddingTop: 10}}>
            - Đăng ký xác nhận kiến thức An toàn thực phẩm
          </TextView>
          <TextView h6 style={{paddingTop: 10}}>
            - Đăng ký xác nhận kiến thức An toàn thực phẩm
          </TextView>
        </View>
      </View>
    );
  }
  render() {
    return (
      <ScrollView style={styles.content}>
        {this.viewItem(
          <IconStar width={SIZE_ICON} hight={SIZE_ICON} />,
          'Đánh giá',
          '4,6/5',
          '56 đánh giá',
          true,
        )}
        {this.viewItem(
          <IconMessageCircle width={SIZE_ICON} hight={SIZE_ICON} />,
          'Tỷ lệ phản hồi chát',
          '80%',
          '15 phút',
        )}
        {this.viewItem(
          <IconFileText width={SIZE_ICON} hight={SIZE_ICON} />,
          'Tỷ lệ hủy đơn',
          '20%',
        )}
        {this.viewItem(
          <IconShop width={SIZE_ICON} hight={SIZE_ICON} />,
          'Sản phẩm',
          '20',
        )}
        {this.viewItem(
          <IconPeople width={SIZE_ICON} hight={SIZE_ICON} />,
          'Đã tham gia',
          '20/10/2020',
        )}

        <View style={{padding: 10}}>
          {this.viewNote()}
          <TextView h4 type="bold" style={{paddingTop: 10}}>
            Giới thiệu
          </TextView>
          <TextView h5 style={{paddingTop: 5, color: COLOR_BG_DARK_GREY}}>
            Xà lách mỡ chứa nhiều chất xơ có lợi cho tiêu hóa, giàu giá trị dinh
            dưỡng, chứa nhiều vitamin và khoáng chất, có công dụng bồi bổ sức
            khỏe, chống oxy hóa, ngăn ngừa ung thư... Xà lách mỡ có thể dùng để
            chế biến nhiều món ăn ngon như luộc, canh, xào, salad... Bạn có thể
            dễ dàng bảo quản sản phẩm nơi khô ráo hoặc ngăn mát tủ lạnh.
          </TextView>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
  viewItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },
  viewLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    paddingHorizontal: 10,
  },
  viewRight: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    paddingHorizontal: 10,
  },
  viewVerify: {
    flexDirection: 'row',
    padding: 10,
    borderColor: COLOR_BG_GREEN,
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: COLOR_BG_GREEN_LIGHT,
  },
});
export default Shop;
