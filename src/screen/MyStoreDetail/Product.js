import React, {Component} from 'react';
import {View, FlatList} from 'react-native';
import {COLOR_BG_WHITE} from '~/AppValues';
import {ItemProduct} from '~/components';
import {dataProduct} from '~/dataTemp';
class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: COLOR_BG_WHITE}}>
        <FlatList
          style={{padding: 10}}
          data={dataProduct}
          numColumns={2}
          renderItem={({item}) => (
            <ItemProduct
              image={item.image}
              title={item.title}
              price={item.price}
              onPress={() => console.log('click', item)}
            />
          )}
          keyExtractor={(item, index) => `${index}`}
        />
      </View>
    );
  }
}

export default Product;
