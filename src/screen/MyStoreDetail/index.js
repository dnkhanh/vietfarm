import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  COLOR_BG_WHITE,
  WIDTH_DEVICE,
  COLOR_BG_GREEN,
  COLOR_BG_BLACK,
  FONT_FAMILY,
} from '~/AppValues';
import {TextView} from '~/components';
import {IconLeft_White, IconDot3White, IconLocation, IconShareWhite} from '~/image';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';
import Shop from './Shop';
import Product from './Product';
const TabScreen = createMaterialTopTabNavigator(
  {
    Shop: {screen: Shop},
    ['Sản phẩm']: {screen: Product},
  },
  {
    tabBarPosition: 'top',
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      upperCaseLabel: false,
      activeTintColor: COLOR_BG_GREEN,
      inactiveTintColor: COLOR_BG_BLACK,
      style: {
        backgroundColor: COLOR_BG_WHITE,
        paddingBottom: 0,
        marginBottom: 0,
      },
      indicatorStyle: {
        borderBottomColor: COLOR_BG_GREEN,
        borderBottomWidth: 2,
      },
      labelStyle: {
        textAlign: 'center',
        fontSize: 14,
        fontFamily: FONT_FAMILY,
      },
    },
  },
);
const TabContent = createAppContainer(TabScreen);
class index extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const {navigation} = this.props;

    return (
      <View style={styles.content}>
        <ImageBackground
          source={require('~/image/banner.png')}
          style={{
            width: WIDTH_DEVICE,
            height: 140,
          }}>
          <View style={styles.viewHeader}>
            <View style={{flexDirection: 'row', padding: 10}}>
              <TouchableOpacity
                style={{flex: 1}}
                onPress={() => navigation.goBack()}>
                <IconLeft_White width={30} height={30} />
              </TouchableOpacity>

              <TouchableOpacity style={{paddingHorizontal: 10}}>
                <IconShareWhite width={24} height={24} />
              </TouchableOpacity>
              <IconDot3White width={24} height={24} />
            </View>
            <View style={{flex: 1}} />
            <View style={styles.ViewInfo}>
              <Image
                source={require('~/image/img_post_1.png')}
                style={styles.avatar}
              />
              <View style={styles.viewName}>
                <TextView h4 style={{color: COLOR_BG_WHITE}}>
                  Trai rau sach GoDream
                </TextView>
                <View style={styles.viewLocation}>
                  <IconLocation height={15} width={15} />
                  <TextView
                    h7
                    style={{color: COLOR_BG_WHITE, paddingHorizontal: 5}}>
                    Vinh
                  </TextView>
                </View>
              </View>
            </View>
          </View>
        </ImageBackground>
        <TabContent />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
  viewHeader: {
    position: 'absolute',
    zIndex: 2,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  ViewInfo: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    paddingBottom: 15,
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#fff',
  },
  viewName: {
    paddingHorizontal: 10,
  },
  viewLocation: {
    flexDirection: 'row',
    paddingTop: 5,
  },
});
export default index;
