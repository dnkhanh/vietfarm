import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Image,
} from 'react-native';
import {Header, Button} from '~/components';
import {
  WIDTH_DEVICE,
} from '~/AppValues';

import {getString} from '~/Localizations/i18n';
import OrderDetail from './OrderDetail';
import { formatNumber } from '~/helpers/format';
export default class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // amount: '10',
      // date_order: moment().format('YYYY-MM-DD'),
    };
  }
  render() {
    const {navigation} = this.props;
    return (
      <View style={{flex: 1}}>
        <Header title="" navigation={navigation} back share option />
        <View style={{width: '100%', height: WIDTH_DEVICE - 150}}>
          <Image
            style={{width: '100%', height: '100%'}}
            source={require('~/image/img_post_1.png')}
          />
        </View>
        <ScrollView style={{paddingHorizontal: 10}}>
          <OrderDetail
            name="Xa lach xach tai"
            price={formatNumber(10000)}
            rating={3}
            amount={3}
            comment={10}
            onChangedData={(data) => console.warn(data)}
            onClose={(data) => console.warn(data)}
          />
        </ScrollView>
        <Button simple title={getString('order.text_continue')} />
      </View>
    );
  }
}
