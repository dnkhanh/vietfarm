import React, {Component} from 'react';
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {TextView, Rating} from '~/components';
import PropTypes from 'prop-types';

import {
  WIDTH_DEVICE,
  COLOR_BG_RED,
  COLOR_BG_WHITE,
  COLOR_BG_GREY,
  COLOR_BG_DARK_GREY,
} from '~/AppValues';
import moment from 'moment';
import {IconX, IconMinus, IconPlus} from '~/image';
import DatePicker from 'react-native-datepicker';

const NumericInput = ({number, decrease, increase}) => (
  <View style={styles.amount}>
    <View style={{flex: 0.2, alignItems: 'center'}}>
      <TouchableOpacity
        disabled={Number(number) <= 1}
        style={styles.deCrease}
        onPress={() => decrease()}>
        <IconMinus width={22} height={22} />
      </TouchableOpacity>
    </View>
    <View style={{flex: 0.6, alignItems: 'center'}}>
      <TextView style={{paddingHorizontal: 30}}>{number}</TextView>
    </View>
    <View style={{flex: 0.2, alignItems: 'center'}}>
      <TouchableOpacity style={styles.inCrease} onPress={() => increase()}>
        <IconPlus width={22} height={22} />
      </TouchableOpacity>
    </View>
  </View>
);
import {getString} from '~/Localizations/i18n';
import { formatNumber } from '~/helpers/format';
export default class OrderDetail extends Component {
  static propTypes = {
    image: PropTypes.string,
    name: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    rating: PropTypes.number.isRequired,
    comment: PropTypes.number,
    amount: PropTypes.number.isRequired,
    onChangedData: PropTypes.func.isRequired,
    onClose: PropTypes.func,
    hideClose: PropTypes.bool,
  };

  static defaultProps = {
    image:
      'https://cdn-www.vinid.net/2019/09/Nen-chon-loai-hoa-qua-bieu-Tet-2019-nao.jpg',
    onClose: () => {},
    hideClose: false,
    comment: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      date_order: moment().format('YYYY-MM-DD'),
      amount: this.props.amount,
    };
  }
  format = (amount) => {
    return Number(amount)
      .toFixed(1)
      .replace(/\d(?=(\d{3})+\.)/g, '$&,');
  };

  onChangedData() {
    if (this.props.onChangedData) {
      this.props.onChangedData({
        date_order: this.state.date_order,
        amount: this.state.amount,
      });
    }
  }
  onCloseItem() {
    if (this.props.onClose) {
      this.props.onClose();
    }
  }
  viewInfoOrder(amount, suggested_price, time_date) {
    const {image, name, price, rating, hideClose, comment} = this.props;
    return (
      <View style={styles.viewInfo}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 0.35, paddingRight: 10}}>
            <Image source={{uri: image}} style={styles.img} />
          </View>
          <View style={{flex: 0.65}}>
            <TextView h4 type="bold">
              {name}
            </TextView>
            <TextView
              h6
              type="bold"
              style={{color: COLOR_BG_RED, paddingVertical: 5}}>
              {formatNumber(price)}
            </TextView>
            <View
              style={{
                flexDirection: 'row',
                alignContent: 'center',
                alignItems: 'center',
              }}>
              <Rating starSize={20} rating={rating} />
              <TextView h7 type="bold" style={{paddingHorizontal: 5}}>
                {rating}
              </TextView>
              <TextView h7 style={{color: COLOR_BG_DARK_GREY}}>
                ({comment || '0'} {getString('order.text_rate')})
              </TextView>
            </View>
          </View>
          {!hideClose && (
            <TouchableOpacity
              onPress={() => {
                this.onCloseItem();
              }}>
              <IconX />
            </TouchableOpacity>
          )}
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.viewDetail}>
            <TextView h5 style={styles.txtLeft}>
              Số lượng:
            </TextView>
            <TextView h5 style={styles.txtLeft}>
              Giá đề xuất:
            </TextView>
            <TextView h5 style={[styles.txtLeft, {paddingTop: 27}]}>
              Ngày nhận hàng:
            </TextView>
          </View>
          <View style={styles.viewDetailRight}>
            <View style={{width: 150}}>
              <View style={{paddingVertical: 5}}>
                <NumericInput
                  number={amount}
                  increase={() => {
                    this.setState({amount: Number(amount) + 1}, () =>
                      this.onChangedData(),
                    );
                  }}
                  decrease={() => {
                    this.setState({amount: Number(amount) - 1}, () =>
                      this.onChangedData(),
                    );
                  }}
                />
              </View>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View style={{width: 150}}>
                <View
                  style={{
                    paddingVertical: 5,
                    width: '100%',
                    alignContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TextView
                    h5
                    style={[
                      styles.txtRight,
                      styles.inputOrder,
                      {width: '100%'},
                    ]}>
                    {this.format(suggested_price)}
                  </TextView>
                </View>
              </View>
              <TextView style={{paddingLeft: 5}}>/ 1KG</TextView>
            </View>
            <TouchableOpacity onPress={() => this.datePickerRef.onPressDate()}>
              <View style={{flexDirection: 'row'}}>
                <View style={{width: 60}}>
                  <View style={{paddingVertical: 5, paddingRight: 5}}>
                    <TextView h5 style={[styles.txtRight, styles.inputOrder]}>
                      {time_date.day}
                    </TextView>
                  </View>
                </View>
                <View style={{width: 95}}>
                  <View style={{paddingVertical: 5, paddingRight: 5}}>
                    <TextView h5 style={[styles.txtRight, styles.inputOrder]}>
                      Tháng {time_date.month}
                    </TextView>
                  </View>
                </View>
                <View style={{width: 65}}>
                  <View style={{paddingVertical: 5, paddingRight: 5}}>
                    <TextView h5 style={[styles.txtRight, styles.inputOrder]}>
                      {time_date.year}
                    </TextView>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
  render() {
    const {price} = this.props;
    const {date_order, amount} = this.state;
    var suggested_price = amount * price;
    var start_date_order = moment().format('YYYY-MM-DD');
    var end_date_order = moment().add(90, 'days').format('YYYY-MM-DD'); // end in 3 months
    let time_date = {
      day: moment(date_order).format('DD'),
      month: moment(date_order).format('MM'),
      year: moment(date_order).format('YYYY'),
    };
    return (
      <View>
        {this.viewInfoOrder(amount, suggested_price, time_date)}
        <DatePicker
          minDate={start_date_order}
          maxDate={end_date_order}
          confirmBtnText="Chọn"
          cancelBtnText="Bỏ qua"
          showIcon={false}
          ref={(ref) => (this.datePickerRef = ref)}
          style={{width: 0, height: 0}}
          mode="date"
          date={date_order}
          customStyles={{
            dateInput: {
              borderWidth: 0,
            },
          }}
          onDateChange={(date) => {
            this.setState({date_order: date}, () => this.onChangedData());
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  amount: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLOR_BG_WHITE,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ccc',
  },
  deCrease: {
    borderRightWidth: 1,
    borderColor: '#ccc',
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  inCrease: {
    borderLeftWidth: 1,
    borderColor: '#ccc',
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  inputOrder: {
    borderColor: COLOR_BG_GREY,
    borderWidth: 1,
    paddingVertical: 12,
    borderRadius: 5,
  },
  paddingVe: {
    paddingVertical: 10,
  },
  viewInfo: {
    paddingTop: 10,
  },
  viewDetail: {
    paddingTop: 5,
    width: WIDTH_DEVICE / 3.2,
    alignItems: 'flex-end',
    alignContent: 'center',
    flex: 0.35,
  },
  viewDetailRight: {
    paddingHorizontal: 10,
    flex: 0.65,
  },
  img: {
    height: WIDTH_DEVICE / 4.5,
    width: '100%',
    borderRadius: 10,
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#fff',
  },
  txtLeft: {
    paddingTop: 18,
    paddingVertical: 10,
  },
  txtRight: {
    paddingHorizontal: 5,
    paddingVertical: 10,
  },
});
