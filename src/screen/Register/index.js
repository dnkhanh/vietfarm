import React, {Component} from 'react';
import {View, ScrollView, TouchableOpacity, StyleSheet} from 'react-native';
import {IconX, IconEye, IconUserName, IconLock} from '~/image';
import {
  COLOR_BG_WHITE,
  PADDING_HEADER,
  COLOR_BG_DARK_GREY,
  IS_PHONE_X,
  COLOR_BG_DARK_GREY_LIGHT,
  SIZE_H3,
  TYPE_MODAL_DANGER,
  TYPE_MODAL_WARNING,
} from '~/AppValues';
import {TextView, InputLine, Button} from '~/components';
import {regexEmail, regexPhone, regexPassword} from '~/helpers/validates';
import {userReg} from '~/redux/actions/auth';
import {showModal} from '~/redux/actions/anotherActions';
import {getString, setLocale} from '~/Localizations/i18n';
import {connect} from 'react-redux';
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secureTextEntry: true,
      email: null,
      phone: null,
      pass: null,
      repass: null,
    };
  }

  onSubmit() {
    const {userReg, showModal} = this.props;
    const {email, pass} = this.state;
    const checkSubmit = this.checkSubmit();
    if (!checkSubmit) {
      userReg(email, pass);
    } else {
      showModal(checkSubmit, TYPE_MODAL_WARNING);
    }
  }

  checkSubmit() {
    const {email, phone, pass, repass} = this.state;
    if (!email && !phone && !pass && !repass) {
      return getString('register.error');
    }
    const validateEmail = regexEmail(email);
    if (validateEmail) {
      return validateEmail;
    }
    const validatePhone = regexPhone(phone);
    if (validatePhone) {
      return validatePhone;
    }
    const validatePass = regexPassword(pass);
    if (validatePass) {
      return validatePass;
    }
    if (pass !== repass) {
      return getString('register.error_repass');
    }
    return false;
  }

  render() {
    const {navigation} = this.props;
    const {secureTextEntry, email, phone, pass, repass} = this.state;
    return (
      <View style={styles.container}>
        <ScrollView
          style={{
            ...PADDING_HEADER,
            padding: 15,
          }}>
          <TouchableOpacity
            style={{alignItems: 'flex-end'}}
            onPress={() => navigation.goBack()}>
            <IconX width={34} height={34} />
          </TouchableOpacity>

          <View style={styles.title}>
            <TextView h0>{getString('register.title')}</TextView>
          </View>
          <View style={{paddingHorizontal: 15, paddingVertical: 15}}>
            <InputLine
              icon={<IconUserName />}
              styleIcon={styles.paddingRight10}
              value={email}
              placeholder="Email"
              onChangeText={(value) => this.setState({email: value})}
              placeholderTextColor={COLOR_BG_DARK_GREY}
            />
            <InputLine
              icon={<IconUserName />}
              styleIcon={styles.paddingRight10}
              placeholder={getString('register.phone')}
              value={phone}
              placeholderTextColor={COLOR_BG_DARK_GREY}
              onChangeText={(value) => this.setState({phone: value})}
            />
            <InputLine
              icon={<IconLock />}
              styleIcon={styles.paddingRight10}
              iconRight={
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      secureTextEntry: secureTextEntry ? false : true,
                    })
                  }>
                  <IconEye />
                </TouchableOpacity>
              }
              value={pass}
              secureTextEntry={secureTextEntry}
              placeholder={getString('register.pass')}
              placeholderTextColor={COLOR_BG_DARK_GREY}
              onChangeText={(value) => this.setState({pass: value})}
            />
            <InputLine
              icon={<IconLock />}
              iconRight={
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      secureTextEntry: secureTextEntry ? false : true,
                    })
                  }>
                  <IconEye />
                </TouchableOpacity>
              }
              styleIcon={styles.paddingRight10}
              secureTextEntry={secureTextEntry}
              onChangeText={(value) => this.setState({repass: value})}
              value={repass}
              placeholder={getString('register.repass')}
              placeholderTextColor={COLOR_BG_DARK_GREY}
            />
            <View style={{paddingTop: 20}}>
              <Button
                styleText={{fontSize: SIZE_H3}}
                onPress={() => {
                  this.onSubmit();
                }}
                title={getString('register.submit')}
              />
            </View>
          </View>
          <View style={styles.Text_Bottom}>
            <View style={{flexDirection: 'row'}}>
              <TextView style={styles.text_1}>
                {getString('register.haveacc')}
              </TextView>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <TextView style={{color: '#0091FF'}}>
                  {getString('register.loginnow')}
                </TextView>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  paddingRight10: {
    paddingRight: 10,
  },
  container: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
  title: {
    alignItems: 'center',
    paddingVertical: 30,
  },
  Text_Bottom: {
    alignItems: 'center',
    paddingBottom: IS_PHONE_X ? 25 : 2,
  },
  text_1: {
    color: COLOR_BG_DARK_GREY_LIGHT,
    paddingRight: 10,
  },
});
export default connect(null, {userReg, showModal})(Login);
