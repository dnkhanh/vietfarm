import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import ModalBox from 'react-native-modalbox';
import {COLOR_BG_WHITE, SHADOW_STYLE} from '~/AppValues';
import {TextView, Button} from '~/components';
import {IconX} from '~/image';
import OrderDetail from '../Order/OrderDetail';
import {PaymentDetail} from './PaymentDetail';

export const ModalPayment = ({visible, onClosed}) => (
  <ModalBox
    isOpen={visible}
    useNativeDriver={false}
    swipeArea={20}
    position="bottom"
    backdropColor="rgba(0, 0, 0, 0.5)"
    onClosed={() => onClosed()}
    style={styles.modal}>
    <View style={styles.viewModal}>
      <View
        style={{
          flexDirection: 'row',
          padding: 10,
          backgroundColor: COLOR_BG_WHITE,
          ...SHADOW_STYLE,
        }}>
        <TextView h4 type="bold" style={{flex: 1}}>
          Tạo thanh toán
        </TextView>

        <TouchableOpacity onPress={() => onClosed()}>
          <IconX />
        </TouchableOpacity>
      </View>
      <View style={{padding: 10}}>
        <PaymentDetail />
      </View>
      <Button simple title="Tạo thanh toán" />
    </View>
  </ModalBox>
);

const styles = StyleSheet.create({
  modal: {
    height: 'auto',
    maxHeight: '70%',
    width: '100%',
  },

  viewModal: {
    backgroundColor: COLOR_BG_WHITE,
    height: 'auto',
  },
});
