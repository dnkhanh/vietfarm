import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import ModalBox from 'react-native-modalbox';
import {COLOR_BG_WHITE, SHADOW_STYLE} from '~/AppValues';
import {TextView, Button} from '~/components';
import {IconX} from '~/image';
import OrderDetail from '../Order/OrderDetail';

export const ModalOrder = ({visible, onClosed}) => (
  <ModalBox
    isOpen={visible}
    useNativeDriver={false}
    swipeArea={20}
    position="bottom"
    backdropColor="rgba(0, 0, 0, 0.5)"
    onClosed={() => onClosed()}
    style={styles.modal}>
    <View style={styles.viewModal}>
      <View
        style={{
          flexDirection: 'row',
          padding: 10,
          backgroundColor: COLOR_BG_WHITE,
          ...SHADOW_STYLE,
        }}>
        <TextView h4 type="bold" style={{flex: 1}}>
          Tạo đơn hàng
        </TextView>

        <TouchableOpacity onPress={() => onClosed()}>
          <IconX />
        </TouchableOpacity>
      </View>
      <View style={{padding: 10}}>
        <OrderDetail
          name="Xa lach xach tai"
          price={10000}
          rating={3}
          amount={3}
          comment={10}
          hideClose
          onChangedData={(data) => console.warn(data)}
        />
      </View>
      <Button simple title="Tạo đơn hàng" />
    </View>
  </ModalBox>
);

const styles = StyleSheet.create({
  modal: {
    height: 'auto',
    maxHeight: '70%',
    width: '100%',
  },

  viewModal: {
    backgroundColor: COLOR_BG_WHITE,
    height: 'auto',
  },
});
