import React from 'react';
import {View, StyleSheet} from 'react-native';
import {
  COLOR_BG_DARK_GREY,
  COLOR_BG_RED,
} from '~/AppValues';
import {TextView} from '~/components';

export const PaymentDetail = () => (
  <View>
    <View style={{flexDirection: 'row', paddingVertical: 5}}>
      <TextView h5 style={{color: COLOR_BG_DARK_GREY, flex: 1}}>
        Tổng tiền
      </TextView>
      <TextView>10.000.000đ</TextView>
    </View>
    <View style={{flexDirection: 'row', paddingVertical: 5}}>
      <TextView h5 style={{color: COLOR_BG_DARK_GREY, flex: 1}}>
        Đã thanh toán
      </TextView>
      <TextView>6.000.000đ</TextView>
    </View>
    <View style={styles.infoPayment}>
      <TextView h5 style={{color: COLOR_BG_DARK_GREY, flex: 1}}>
        Còn lại
      </TextView>
      <TextView h4 type="bold" style={{color: COLOR_BG_RED}}>
        4.000.000đ
      </TextView>
    </View>
  </View>
);

const styles = StyleSheet.create({
  infoPayment: {
    marginTop: 5,
    flexDirection: 'row',
    paddingVertical: 5,
    borderTopColor: '#C4C4C4',
    borderTopWidth: 1,
  },
});
