import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {
  WIDTH_DEVICE,
  COLOR_BG_DARK_GREY,
  COLOR_BG_WHITE,
  SHADOW_STYLE,
  COLOR_BG_RED,
  COLOR_BG_GREEN,
} from '~/AppValues';
import {TextView, Rating} from '~/components';
import {IconDot3} from '~/image';
export const InfoOrder = ({viewBottom}) => (
  <View style={styles.viewInfo}>
    <View style={{flexDirection: 'row'}}>
      <Image source={require('~/image/banner.png')} style={styles.img} />
      <View style={{flex: 1, paddingHorizontal: 10}}>
        <TextView h4 type="bold">
          Xa lach Da lat
        </TextView>
        <TextView
          h6
          type="bold"
          style={{color: COLOR_BG_RED, paddingVertical: 5}}>
          56.999đ
        </TextView>
        <View style={{flexDirection: 'row'}}>
          <Rating rating={3.5} />
          <TextView h5type="bold" style={{paddingHorizontal: 5}}>
            4.6
          </TextView>
          <TextView h5 style={{color: COLOR_BG_DARK_GREY}}>
            (56)
          </TextView>
        </View>
      </View>
      <IconDot3 />
    </View>
    <View style={{flexDirection: 'row'}}>
      <View style={styles.viewDetail}>
        <TextView h5 style={styles.txtLeft}>
          Số lượng:
        </TextView>
        <TextView h5 style={styles.txtLeft}>
          Giá đề xuất:
        </TextView>
        <TextView h5 style={styles.txtLeft}>
          Ngày nhận hàng:
        </TextView>
      </View>
      <View style={styles.viewDetailRight}>
        <TextView h5 style={styles.txtRight}>
          120kg
        </TextView>
        <TextView h5 style={styles.txtRight}>
          20.000d / 1kg
        </TextView>
        <TextView h5 style={styles.txtRight}>
          30/Tháng 12/2018
        </TextView>
      </View>
    </View>
    {viewBottom}
  </View>
);

const styles = StyleSheet.create({
  viewInfo: {
    backgroundColor: COLOR_BG_WHITE,
    borderRadius: 10,
    ...SHADOW_STYLE,
    padding: 10,
    marginTop: 5,
  },
  viewDetailRight: {
    paddingTop: 5,
    paddingHorizontal: 10,
    flex: 1,
  },
  img: {
    height: WIDTH_DEVICE / 4.5,
    width: WIDTH_DEVICE / 3.2,
    borderRadius: 10,
  },
  viewDetail: {
    paddingTop: 5,
    width: WIDTH_DEVICE / 3.2,
    alignItems: 'flex-end',
  },
  txtLeft: {
    paddingTop: 5,
    color: COLOR_BG_DARK_GREY,
  },
  txtRight: {
    paddingTop: 5,
    color: COLOR_BG_GREEN,
  },
});
