import React from 'react';
import {View, StyleSheet} from 'react-native';
import {
  COLOR_BG_DARK_GREY,
  COLOR_BG_WHITE,
  SHADOW_STYLE,
  COLOR_BG_GREEN,
} from '~/AppValues';
import {TextView} from '~/components';
import {IconVerify} from '~/image';
export const OrderSuccess = () => (
  <View style={styles.viewOrderSuccess}>
    <IconVerify />
    <View style={{flex: 1, paddingHorizontal: 10}}>
      <TextView h6 type="bold" style={{color: COLOR_BG_GREEN}}>
        Đặt cọc thành công
      </TextView>
      <TextView h7 style={{color: COLOR_BG_DARK_GREY, paddingTop: 5}}>
        Bạn đã đặt cọc 50% (3.000.000đ) tổng giá trị đơn hàng #545643
      </TextView>
    </View>
  </View>
);

const styles = StyleSheet.create({
  viewOrderSuccess: {
    marginTop: 5,
    flexDirection: 'row',
    borderRadius: 10,
    padding: 10,
    backgroundColor: COLOR_BG_WHITE,
    ...SHADOW_STYLE,
  },
});
