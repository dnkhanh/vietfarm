import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  Platform,
  Keyboard,
} from 'react-native';
import {
  COLOR_GRAY,
  COLOR_BG_DARK_GREY,
  COLOR_BG_WHITE,
  SHADOW_STYLE,
  COLOR_BG_GREEN,
  FONT_FAMILY,
  COLOR_BG_GREY,
  COLOR_BG_BLACK,
  IS_PHONE_X,
} from '~/AppValues';
import {TextView, HeaderChat, Button} from '~/components';
import {IconPlusWhite, IconSend, IconVerify} from '~/image';
import {ModalMore} from './ModalMore';
import {ModalPayment} from './ModalPayment';
import {PaymentDetail} from './PaymentDetail';
import {InfoOrder} from './InfoOrder';
import {OrderSuccess} from './OrderSuccess';
import {ModalOrder} from './ModalOrder';

class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textSend: undefined,
      bodyChat: [],
      isModalMore: false,
      isModalOrder: false,
      isModalPayment: false,
    };
  }

  componentDidMount() {
    setTimeout(
      () =>
        this.setState({
          bodyChat: [
            {type: 'receive', content: '', image: true},
            {type: 'receive', content: ' Quý khách muốn gì ở em?'},
          ],
        }),
      3000,
    );
  }

  componentWillUnmount() {
    clearInterval(this._interval);
  }

  autoChat() {
    let time = 0;
    this._interval = setInterval(() => {
      let content;
      switch (time) {
        case 1:
          content = {
            type: 'meCreateOrder',
            content: 'Phạm Anh đã tạo một đơn hàng xà lách (#435435)',
          };
          break;

        case 0:
          content = {
            type: 'receive',
            content:
              'Xin chào quý khách hàng! Xin khách chờ xíu, Nhân viên dưới đây sẽ tạo đơn rồi trực tiếp ship đến cho bạn!',
            image: require('~/image/banner.png'),
          };
          break;
        case 2:
          content = {
            type: 'userCreateOrder',
            content: 'Xa lach Da lat đã tạo một đơn hàng xà lách (#435435)',
          };
          break;
        case 3:
          content = {
            type: 'userCreatePayment',
            content: 'Xa lach Da lat đã tạo một đơn hàng xà lách (#435435)',
          };
          break;

        case 4:
          content = {type: 'paymentSuccess', content: 'Đơn hàng đã được tạo'};
          break;
        case 5:
          content = {type: 'paymentDetail'};
          break;
        case 6:
          content = {type: 'reader'};
          clearInterval(this._interval);
          break;

        default:
          break;
      }
      this.setState({
        bodyChat: [{...content}, ...this.state.bodyChat],
      });
      time = time + 1;
    }, 4000);
  }

  bodyChat(item) {
    switch (item.type) {
      case 'send':
        return this.viewSendChat(item.content, item.image);
      case 'receive':
        return this.viewReceiveChat(item.content, true, item.image);

      case 'meCreateOrder': {
        return (
          <>
            {this.viewNote(item.content)}
            <InfoOrder />
          </>
        );
      }
      case 'userCreateOrder': {
        return (
          <>
            {this.viewNote(item.content)}
            <InfoOrder
              viewBottom={
                <View style={{flexDirection: 'row', paddingTop: 10}}>
                  <Button
                    title="Đồng ý"
                    onPress={() => this.setState({isModalOrder: true})}
                    style={{
                      flex: 1,
                      marginRight: 10,
                      backgroundColor: COLOR_BG_GREY,
                    }}
                    styleText={{color: COLOR_BG_BLACK}}
                  />
                  <Button title="Mặc cả" style={{flex: 1, marginLeft: 10}} />
                </View>
              }
            />
          </>
        );
      }

      case 'userCreatePayment': {
        return (
          <>
            {this.viewNote(item.content)}
            <InfoOrder
              viewBottom={
                <Button
                  title="Đồng ý"
                  style={{marginTop: 10}}
                  onPress={() => this.setState({isModalPayment: true})}
                />
              }
            />
          </>
        );
      }

      case 'paymentSuccess': {
        return (
          <>
            {this.viewNote(item.content)}
            <OrderSuccess />
          </>
        );
      }

      case 'paymentDetail': {
        return (
          <View style={styles.viewPayment}>
            <PaymentDetail />
            <Button title="Tạo thanh toán" />
          </View>
        );
      }
      case 'reader': {
        return this.viewHasRead();
      }
      default:
        return null;
    }
  }

  viewSendChat(message, image) {
    return (
      <View style={{alignItems: 'flex-end', marginVertical: 3}}>
        <View style={styles.viewSendChat}>
          {message ? (
            <TextView h5 style={{color: COLOR_BG_WHITE}}>
              {message}
            </TextView>
          ) : null}
          {image && (
            <Image
              source={require('~/image/banner.png')}
              style={{height: 70, width: 110, borderRadius: 5}}
            />
          )}
        </View>
      </View>
    );
  }

  viewReceiveChat(message, showIcon, image) {
    return (
      <View style={styles.viewContentReceive}>
        <View style={{alignSelf: 'flex-end', width: 33}}>
          {showIcon && (
            <Image
              source={require('~/image/ngoctrinh.jpg')}
              style={{height: 30, width: 30, borderRadius: 15}}
            />
          )}
        </View>
        <View style={styles.viewReceiveChat}>
          {message ? <TextView h5>{message}</TextView> : null}
          {image && (
            <Image
              source={require('~/image/ngoctrinh.jpg')}
              style={{height: 70, width: 110, borderRadius: 5}}
            />
          )}
        </View>
      </View>
    );
  }
  viewHasRead() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
          flexDirection: 'row',
          marginTop: 5,
        }}>
        {[{id: 1}, {id: 2}].map((i, index) => (
          <Image
            key={index}
            source={require('~/image/ngoctrinh.jpg')}
            style={{height: 14, width: 14, borderRadius: 7, marginLeft: 7}}
          />
        ))}
      </View>
    );
  }
  viewBottom() {
    return (
      <View style={styles.viewBottom}>
        <TouchableOpacity
          style={styles.viewIconPlus}
          onPress={() => {
            this.setState({isModalMore: true});
          }}>
          <IconPlusWhite width={20} height={20} />
        </TouchableOpacity>
        <View style={styles.viewInput}>
          <TextView h5 style={{color: COLOR_BG_DARK_GREY}}>
            Aa
          </TextView>
          <TextInput
            multiline
            style={styles.textInput}
            value={this.state.textSend}
            onChangeText={(value) => this.setState({textSend: value})}
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            if (!this.state.textSend) {
              return;
            }
            this.setState({
              bodyChat: [
                {type: 'send', content: this.state.textSend},
                ...this.state.bodyChat,
              ],
              textSend: undefined,
            });
            setTimeout(() => this.autoChat(), 5000);
          }}>
          <IconSend />
        </TouchableOpacity>
      </View>
    );
  }

  viewNote(content) {
    return (
      <TextView
        h7
        style={{
          color: COLOR_BG_DARK_GREY,
          paddingTop: 10,
          paddingBottom: 5,
          textAlign: 'center',
        }}>
        {content}
      </TextView>
    );
  }

  onClosedModal() {
    this.setState({isModalMore: false});
  }

  render() {
    const {navigation} = this.props;
    const {isModalMore, isModalPayment, isModalOrder} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: COLOR_GRAY}}>
        <View style={{backgroundColor: COLOR_BG_WHITE}}>
          <HeaderChat
            title="Xa lach da lat"
            time="online 12 phut truoc"
            navigation={this.props.navigation}
          />
        </View>
        <FlatList
          style={{flex: 1}}
          contentContainerStyle={{paddingVertical: 15, paddingHorizontal: 10}}
          inverted
          data={this.state.bodyChat}
          renderItem={({item, index}) => (
            <View>{this.bodyChat(item, index)}</View>
          )}
          keyExtractor={(item, index) => `${index}`}
        />
        {this.viewBottom()}
        <ModalMore
          visible={isModalMore}
          onClosed={() => this.setState({isModalMore: false})}
          onCreateOrder={() => console.warn('chon create order')}
          onRateOrder={() => navigation.navigate('Reviews')}
          onAddImage={() => console.warn('chon add image')}
        />
        <ModalOrder
          visible={isModalOrder}
          onClosed={() => this.setState({isModalOrder: false})}
        />
        <ModalPayment
          visible={isModalPayment}
          onClosed={() => this.setState({isModalPayment: false})}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: 'red',
  },
  viewBody: {
    flex: 1,
    backgroundColor: COLOR_GRAY,
    padding: 10,
  },

  viewPayment: {
    marginTop: 5,
    borderRadius: 10,
    padding: 10,
    backgroundColor: COLOR_BG_WHITE,
    ...SHADOW_STYLE,
  },

  viewSendChat: {
    backgroundColor: COLOR_BG_GREEN,
    borderRadius: 15,
    paddingHorizontal: 12,
    paddingVertical: 7,
    maxWidth: '80%',
  },
  viewContentReceive: {
    alignItems: 'flex-start',
    marginVertical: 3,
    flexDirection: 'row',
  },
  viewReceiveChat: {
    backgroundColor: COLOR_BG_WHITE,
    borderRadius: 15,
    paddingHorizontal: 12,
    paddingVertical: 7,
    maxWidth: '80%',
  },
  viewBottom: {
    flexDirection: 'row',
    backgroundColor: COLOR_BG_WHITE,
    borderTopWidth: 1,
    borderTopColor: '#E9EAED',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingBottom: IS_PHONE_X ? 15 : 3,
  },
  viewIconPlus: {
    backgroundColor: COLOR_BG_GREEN,
    height: 36,
    width: 36,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewInput: {
    flex: 1,
    marginHorizontal: 5,
    backgroundColor: COLOR_GRAY,
    borderRadius: 20,
    marginVertical: 5,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    // height: Platform.OS === 'ios' ? 45 : 'auto',
  },

  textInput: {
    flex: 1,
    maxHeight: 120,
    fontFamily: FONT_FAMILY,
    fontSize: Platform.OS === 'android' ? 14 : 16,
    padding: 5,
    height: '100%',
    marginVertical: Platform.OS === 'ios' ? 10 : 2,
  },
});
export default Chat;
