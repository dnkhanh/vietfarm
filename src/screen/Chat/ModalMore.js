import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import ModalBox from 'react-native-modalbox';
import {COLOR_BG_WHITE, COLOR_BG_GREY} from '~/AppValues';
import {TextView} from '~/components';
import {IconCreateOrder, IconRateOrder, IconAddImage} from '~/image';

export const ModalMore = ({
  visible,
  onClosed,
  onCreateOrder,
  onRateOrder,
  onAddImage,
}) => (
  <ModalBox
    isOpen={visible}
    useNativeDriver={false}
    swipeArea={20}
    position="bottom"
    backdropColor="rgba(0, 0, 0, 0.5)"
    onClosed={() => onClosed()}
    style={styles.modal}>
    <View style={styles.viewModal}>
      <TouchableOpacity
        style={styles.btnModal}
        onPress={() => {
          onClosed();
          onCreateOrder();
        }}>
        <View style={styles.iconModal}>
          <IconCreateOrder />
        </View>
        <TextView h5>Tạo đơn hàng</TextView>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.btnModal}
        onPress={() => {
          onClosed();
          onRateOrder();
        }}>
        <View style={styles.iconModal}>
          <IconRateOrder />
        </View>
        <TextView h5>Đánh giá</TextView>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.btnModal}
        onPress={() => {
          onClosed();
          onAddImage();
        }}>
        <View style={styles.iconModal}>
          <IconAddImage />
        </View>
        <TextView h5>Thêm hình ảnh</TextView>
      </TouchableOpacity>
    </View>
  </ModalBox>
);

const styles = StyleSheet.create({
  modal: {
    height: 'auto',
    maxHeight: '70%',
    width: '100%',
  },

  viewModal: {
    backgroundColor: COLOR_BG_WHITE,
    height: 'auto',
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 20,
  },
  btnModal: {alignItems: 'center'},
  iconModal: {
    height: 50,
    width: 50,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: COLOR_BG_GREY,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
});
