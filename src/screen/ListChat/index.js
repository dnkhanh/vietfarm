import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

import {TextView, Header} from '~/components';
import {COLOR_BG_WHITE, COLOR_BG_BLACK, COLOR_BG_DARK_GREY} from '~/AppValues';
import {getString} from '~/Localizations/i18n';
class ListChat extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  ViewItemChat(read) {
    const {navigation} = this.props;
    return (
      <TouchableOpacity
        style={{flexDirection: 'row', padding: 10}}
        onPress={() => navigation.navigate('ChatScreen')}>
        <Image
          source={require('~/image/ngoctrinh.jpg')}
          style={{width: 50, height: 50, borderRadius: 25}}
        />
        <View style={{flex: 1, paddingHorizontal: 5}}>
          <TextView
            h5
            type={read ? '' : 'bold'}
            rr
            style={{color: COLOR_BG_BLACK, paddingBottom: 5}}>
            {getString('list_chat.text_order')} #4343- Rau xanh
          </TextView>
          <TextView
            h6
            type={read ? '' : 'bold'}
            style={{color: read ? COLOR_BG_DARK_GREY : COLOR_BG_BLACK}}>
            {getString('list_chat.text_alert')}
          </TextView>
        </View>
        <TextView h7 style={{paddingTop: 5}}>
          10:20
        </TextView>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={styles.content}>
        <Header title={getString('list_chat.header')} option />
        <ScrollView>
          {this.ViewItemChat(false)}
          {this.ViewItemChat(true)}
          {this.ViewItemChat(false)}
          {this.ViewItemChat(true)}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flexDirection: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
});
export default ListChat;
