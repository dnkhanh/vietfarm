import React, {Component} from 'react';
import {View} from 'react-native';
import {Button, TextView, TextInput} from '~/components';
import {
  COLOR_BG_GREEN,
  TYPE_MODAL_SUCCESS,
  TYPE_MODAL_DANGER,
  COLOR_BG_APP,
  COLOR_BG_GREY,
  COLOR_BG_WHITE,
  HEGHT_DEVICE,
} from '~/AppValues';
import {
  showModal,
  NavigationReplace,
  showLoading,
  hideLoading,
} from '~/redux/actions/anotherActions';
import {connect} from 'react-redux';
import {Header} from '~/components';

class Authcodephone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      codeOTP: '',
    };
  }
  async Authsubmit() {
    const {
      dataAuthPhone,
      showModal,
      NavigationReplace,
      showLoading,
      hideLoading,
    } = this.props;
    try {
      await showLoading();
      await dataAuthPhone.confirm(this.state.codeOTP);
      await hideLoading();
      await NavigationReplace('Main');
      console.log('Xác thực thành công');
    } catch (error) {
      await hideLoading();
      await showModal('Mã Code Không Đúng', TYPE_MODAL_DANGER);
      console.log(error);
      console.log('Xác thực thất bại');
    }
  }
  render() {
    const {codeOTP,navigation} = this.state;
    return (
      <View style={{flex: 1, backgroundColor: COLOR_BG_WHITE}}>
        <Header title="Xác thực SĐT" navigation={navigation} back />
        <View
          style={{
            flex: 1,
            marginTop: HEGHT_DEVICE / 6,
            padding: 40,
          }}>
          <TextView
            style={{
              fontSize: 20,
              color: '#3e3e3e',
              marginBottom: 50,
              alignSelf: 'center',
            }}>
            XÁC THỰC SỐ ĐIỆN THOẠI
          </TextView>
          <TextInput
            // style={{marginVertical: 15}}
            placeholder="Mã OTP"
            maxLength={6}
            value={codeOTP}
            onChangeText={(value) => this.setState({codeOTP: value})}
            keyboardType="number-pad"
          />
          <View style={{height: 10}} />

          <Button
            title="Xác thực"
            style={{height: 50}}
            onPress={() => this.Authsubmit()}
          />
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => ({
  dataAuthPhone: state.auth.dataAuthPhone,
});

export default connect(mapStateToProps, {
  showModal,
  NavigationReplace,
  showLoading,
  hideLoading,
})(Authcodephone);
