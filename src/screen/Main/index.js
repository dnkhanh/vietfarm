import React, {Component} from 'react';
import {View} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import {COLOR_BG_GREEN, COLOR_BG_DARK_GREY, FONT_FAMILY} from '~/AppValues';
import {
  IconHome,
  IconUHome,
  IconOffer,
  IconUOffer,
  IconChat,
  IconUChat,
  IconUser,
  IconUUser,
} from '~/image';
import Home from '../Home';
import Suggest from '../Suggest';
import ListChat from '../ListChat';
import Setting from '../Setting';
import {getString} from '~/Localizations/i18n';

const sizeIcon = 22;

function viewIcon(name, active) {
  if (name === ROUTE_NAME.HOME) {
    if (active) {
      return <IconHome width={sizeIcon} height={sizeIcon} />;
    }
    return <IconUHome width={sizeIcon} height={sizeIcon} />;
  }
  if (name === ROUTE_NAME.SUGGEST) {
    if (active) {
      return <IconOffer width={sizeIcon} height={sizeIcon} />;
    }
    return <IconUOffer width={sizeIcon} height={sizeIcon} />;
  }
  if (name === ROUTE_NAME.CHAT) {
    if (active) {
      return <IconChat width={sizeIcon} height={sizeIcon} />;
    }
    return <IconUChat width={sizeIcon} height={sizeIcon} />;
  }
  if (name === ROUTE_NAME.USER) {
    if (active) {
      return <IconUser width={sizeIcon} height={sizeIcon} />;
    }
    return <IconUUser width={sizeIcon} height={sizeIcon} />;
  }
  return <View />;
}
const ROUTE_NAME = {
  HOME: getString('main.home'),
  SUGGEST: getString('main.suggest'),
  CHAT: getString('main.chat'),
  USER: getString('main.account'),
};

const TabNavigator = createBottomTabNavigator(
  {
    [ROUTE_NAME.HOME]: {screen: Home},
    [ROUTE_NAME.SUGGEST]: {screen: Suggest},
    [ROUTE_NAME.CHAT]: {screen: ListChat},
    [ROUTE_NAME.USER]: {screen: Setting},
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        return (
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {viewIcon(routeName, focused)}
          </View>
        );
      },
    }),
    tabBarOptions: {
      style: {
        height: 60,
        borderTopWidth: 0,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
        paddingTop: 6,
        paddingBottom: 6,
      },
      activeTintColor: COLOR_BG_GREEN,
      inactiveTintColor: COLOR_BG_DARK_GREY,
      // showLabel: false,
      labelStyle: {
        fontFamily: FONT_FAMILY,
        fontSize: 13,
      },
    },
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    animationEnabled: true,
    lazy: true,
  },
);

export default TabNavigator;
