import React, {Component} from 'react';
import {View, TouchableOpacity, StyleSheet, Image} from 'react-native';
import {COLOR_BG_RED} from '../../AppValues';
import TextView from '../ViewBasic/TextView';
import {formatNumber} from '~/helpers/format';
import PropTypes from 'prop-types';
class ItemProduct extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {image, title, price} = this.props;
    return (
      <TouchableOpacity
        {...this.props}
        style={{width: '50%', paddingBottom: 10}}>
        <View style={{padding: 5}}>
          <Image style={styles.img_product_home} source={image} />
          <TextView h6 numberOfLines={2} style={{height: 35, marginTop: 5}}>
            {title}
          </TextView>
          <TextView h6 type="bold" style={{color: COLOR_BG_RED}}>
            {formatNumber(price)}
          </TextView>
        </View>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  img_product_home: {
    width: '100%',
    height: 120,
    backgroundColor: '#eee',
    borderRadius: 5,
  },
});
export default ItemProduct;
