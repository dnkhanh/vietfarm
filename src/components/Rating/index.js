import React, {Component} from 'react';
import {Platform} from 'react-native';
import StarRating from 'react-native-star-rating';
import {COLOR_YELLOW, COLOR_DISABLE} from '~/AppValues';

class Rating extends Component {
  render() {
    return (
      <StarRating
        emptyStar="star"
        fullStar="star"
        halfStar="star-half"
        iconSet="MaterialIcons"
        maxStars={5}
        starSize={Platform.OS === 'ios' ? 25 : 20}
        fullStarColor={COLOR_YELLOW}
        emptyStarColor={COLOR_DISABLE}
        halfStarColor={COLOR_YELLOW}
        disabled
        rating={this.props.rating}
        {...this.props}
      />
    );
  }
}

export default Rating;
