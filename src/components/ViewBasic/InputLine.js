import React, {Component} from 'react';
import RN, {View, StyleSheet, Platform} from 'react-native';
import {COLOR_BORDER_TEXT_INPUT, COLOR_BG_GREY} from '../../AppValues';
import {TextView} from '~/components';
class InputLine extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const {icon, title, iconRight, style, styleIcon, styleInput} = this.props;
    return (
      <View style={{flex:1,paddingVertical: 10}}>
        {title && (
          <View>
            <TextView h6>{title}</TextView>
          </View>
        )}
        <View style={[styles.content, style]}>
          {icon && (
            <View
              style={{justifyContent: 'center', paddingLeft: 5, ...styleIcon}}>
              {icon}
            </View>
          )}
          <RN.TextInput
            placeholderTextColor={COLOR_BG_GREY}
            {...this.props}
            style={[styles.txt_input, styleInput]}
          />
          {iconRight && (
            <View
              style={{
                justifyContent: 'center',
                paddingHorizontal: 5,
                ...styleIcon,
              }}>
              {iconRight}
            </View>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flexDirection: 'row',
    borderColor: COLOR_BORDER_TEXT_INPUT,
    borderBottomWidth: 1,
    flex: 1,
    height: Platform.OS === 'ios' ? 45 : 'auto',
  },
  txt_input: {
    height: '100%',
    color: 'black',
    flex: 1,
    fontSize: Platform.OS === 'ios' ? 19 : 17,
    fontFamily: Platform.OS === 'android' ? 'Roboto-Regular' : 'Roboto',
  },
});

export default InputLine;
