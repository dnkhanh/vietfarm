import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {
  COLOR_BG_WHITE,
  COLOR_BG_GREEN,
  COLOR_BG_RED,
  COLOR_BG_DARK_GREY_LIGHT,
  COLOR_BG_APP,
} from '../../AppValues';
import TextView from './TextView';

class MyAlert extends Component {
  render() {
    const {text, type} = this.props;
    switch (type) {
      case 'danger': {
        return (
          <View style={{alignItems: 'center'}}>
            <TextView style={styles.danger}>{text}</TextView>
          </View>
        );
      }
      case 'success': {
        return (
          <View style={{alignItems: 'center'}}>
            <TextView style={styles.success}>{text}</TextView>
          </View>
        );
      }
      case 'noti': {
        return (
          <View style={{alignItems: 'center'}}>
            <TextView style={styles.noti}>{text}</TextView>
          </View>
        );
      }
      default: {
        return (
          <View style={{alignItems: 'center'}}>
            <TextView style={styles.default}>{text}</TextView>
          </View>
        );
      }
    }
  }
}

const styles = StyleSheet.create({
  default: {
    backgroundColor: COLOR_BG_APP,
    padding: 10,
    color: COLOR_BG_WHITE,
    opacity: 0.8,
  },
  danger: {
    backgroundColor: COLOR_BG_RED,
    padding: 10,
    color: COLOR_BG_WHITE,
    opacity: 0.8,
  },
  success: {
    backgroundColor: COLOR_BG_GREEN,
    padding: 10,
    color: COLOR_BG_WHITE,
    opacity: 0.8,
  },
  noti: {
    backgroundColor: COLOR_BG_DARK_GREY_LIGHT,
    padding: 10,
    color: COLOR_BG_WHITE,
    opacity: 0.8,
  },
});

export default MyAlert;
