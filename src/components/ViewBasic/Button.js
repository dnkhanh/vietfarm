import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet, Platform} from 'react-native';
import {
  COLOR_BG_GREY,
  COLOR_BG_WHITE,
  COLOR_BG_GREEN,
  IS_PHONE_X,
} from '../../AppValues';
import Text from './TextView';

class Button extends Component {
  render() {
    const {title, light, style, simple, styleText} = this.props;
    if (simple) {
      return (
        <TouchableOpacity {...this.props} style={[styles.viewSimple, style]}>
          <Text h5 type="bold" style={{color: COLOR_BG_WHITE, padding: 10}}>
            {title}
          </Text>
        </TouchableOpacity>
      );
    }
    if (light) {
      return (
        <TouchableOpacity {...this.props} style={[styles.button, style]}>
          <Text h5 style={[styles.text, {...styleText}]}>
            {title}
          </Text>
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity {...this.props} style={[styles.container, style]}>
        <Text h5 type="bold" style={[styles.text2, {...styleText}]}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 5,
    backgroundColor: '#fff',
    justifyContent: 'center',
    textAlign: 'center',
    // marginRight: 10,
    borderColor: COLOR_BG_GREY,
    paddingHorizontal: 15,
    borderWidth: 1,
    height: Platform.OS === 'ios' ? 60 : 40,
  },
  text: {
    color: 'black',
    fontSize: 14,
  },
  container: {
    borderRadius: 5,
    backgroundColor: '#00C95B',
    textAlign: 'center',
    paddingHorizontal: 10,
    justifyContent: 'center',
    // marginRight: 10,
    height: Platform.OS === 'ios' ? 60 : 40,
  },
  text2: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
  },
  viewSimple: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR_BG_GREEN,
    paddingBottom: IS_PHONE_X ? 12 : 2,
  },
});

export default Button;
