import React, {Component} from 'react';
import RN, {View, StyleSheet, Platform} from 'react-native';
import {COLOR_BORDER_TEXT_INPUT, COLOR_BG_GREY} from '../../AppValues';
class TextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const {icon, iconRight, style, styleInput} = this.props;
    return (
      <View style={[styles.content, style]}>
        {icon && (
          <View style={{justifyContent: 'center', paddingLeft: 5}}>{icon}</View>
        )}
        <RN.TextInput
          placeholderTextColor={COLOR_BG_GREY}
          style={[styleInput,styles.txt_input]}
          {...this.props}
        />
        {iconRight && (
          <View style={{justifyContent: 'center', paddingHorizontal: 5}}>
            {iconRight}
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'row',
    borderColor: COLOR_BORDER_TEXT_INPUT,
    borderWidth: 1,
    height: Platform.OS === 'ios' ? 55 : 'auto',
  },
  txt_input: {
    height: '100%',
    color: 'black',
    flex: 1,
    marginRight: 10,
    marginLeft: 10,
    fontSize: Platform.OS === 'ios' ? 15 : 13,
    fontFamily: Platform.OS === 'android' ? 'Roboto-Regular' : 'Roboto',
  },
});

export default TextInput;
