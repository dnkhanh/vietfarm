import React, {Component} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {COLOR_BG_WHITE, SHADOW_STYLE, PADDING_HEADER} from '~/AppValues';
import {IconLeft, IconDot3, IconShare,IconX} from '~/image';
import TextView from '../ViewBasic/TextView';

class Header extends Component {
  render() {
    const {navigation, title, back, share,exit, option} = this.props;
    return (
      <View style={[styles.flexDirection, styles.viewHeader]}>
        {back && (
          <TouchableOpacity
            style={{paddingHorizontal: 10}}
            onPress={() => navigation.goBack()}>
            <IconLeft width={25} height={25} />
          </TouchableOpacity>
        )}
        <TextView
          h3
          type="bold"
          style={{flex: 1, color: '#333537', paddingHorizontal: 10}}>
          {title}
        </TextView>
        {exit && (
          <TouchableOpacity
            style={{paddingHorizontal: 10}}
            onPress={() => navigation.goBack()}>
            <IconX width={25} height={25} />
          </TouchableOpacity>
        )}
        {share && (
          <TouchableOpacity
            style={{paddingHorizontal: 10}}
            onPress={() => {
              try {
                option();
              } catch (error) {
                console.warn('option not function');
              }
            }}>
            <IconShare width={25} height={25} />
          </TouchableOpacity>
        )}
        {option && (
          <TouchableOpacity
            style={{paddingHorizontal: 10}}
            onPress={() => {
              try {
                option();
              } catch (error) {
                console.warn('option not function');
              }
            }}>
            <IconDot3 width={25} height={25} />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  flexDirection: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    backgroundColor: COLOR_BG_WHITE,
  },
  viewHeader: {
    backgroundColor: COLOR_BG_WHITE,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
    ...SHADOW_STYLE,
    ...PADDING_HEADER,
  },
});
export default Header;
