import React, {Component} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {COLOR_BG_WHITE, SHADOW_STYLE, PADDING_HEADER} from '~/AppValues';
import {IconLeft, IconDot3} from '~/image';
import TextView from '../ViewBasic/TextView';

class HeaderChat extends Component {
  render() {
    const {navigation, title, option, time} = this.props;
    return (
      <View style={[styles.flexDirection, styles.viewHeader]}>
        <TouchableOpacity
          style={{paddingHorizontal: 10}}
          onPress={() => navigation.goBack()}>
          <IconLeft width={25} height={25} />
        </TouchableOpacity>

        <View style={{flex: 1, paddingHorizontal: 10,}}>
          <TextView h3 type="bold" style={{ color: '#333537'}}>
            {title}
          </TextView>
          <TextView h7 style={{ color: '#7B7B81',}}>
            {time}
          </TextView>
        </View>

        <TouchableOpacity
          style={{paddingHorizontal: 10}}
          onPress={() => {
            try {
              option();
            } catch (error) {
              console.warn('option not function');
            }
          }}>
          <IconDot3 width={25} height={25} />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  flexDirection: {
    flexDirection: 'row',
  },
  viewHeader: {
    backgroundColor: COLOR_BG_WHITE,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    ...SHADOW_STYLE,
    ...PADDING_HEADER,
  },
});
export default HeaderChat;
