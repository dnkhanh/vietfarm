import Header from './Header';
import HeaderChat from './Header/HeaderChat';
//view base
import Button from './ViewBasic/Button';
import TextArea from './ViewBasic/TextArea';
import TextInput from './ViewBasic/TextInput';
import TextView from './ViewBasic/TextView';
import InputLine from './ViewBasic/InputLine';
import MyAlert from './ViewBasic/Alert';
import Rating from './Rating';
import ActionSheet from './Actionsheet';

export {
  Header,
  HeaderChat,
  Button,
  TextArea,
  TextInput,
  TextView,
  InputLine,
  MyAlert,
  ActionSheet,
};

//ItemProduct

import ItemProduct from './ItemProduct';

export {ItemProduct, Rating};
