export const dataProduct = [
  {
    id: '1',
    title: 'Thịt lợn mán',
    image:
      require('~/image/Product/pork.jpeg'),
    price: '56000 Đ',
  },
  {
    id: '2',
    title: 'Củ cải đỏ Hà Lan',
    image:
    require('~/image/Product/radish.jpeg'),
    price: '40000 Đ',
  },
  {
    id: '3',
    title: 'Súp lơ Đà Lạt',
    image:
    require('~/image/Product/cauliflower.jpeg'),
    price: '54000 Đ',
  },
  {
    id: '4',
    title: 'Trứng gà loại 1',
    image:
    require('~/image/Product/egg.jpeg'),
    price: '76000 Đ',
  },
  {
    id: '5',
    title: 'Cá bạc má',
    image:
    require('~/image/Product/fish.jpeg'),
    price: '100000 Đ',
  },
  {
    id: '6',
    title: 'Bí đỏ',
    image:
    require('~/image/Product/pumkin.jpeg'),
    price: '70000 Đ',
  },
  {
    id: '7',
    title: 'Cà chua bi Đà Lạt',
    image:
    require('~/image/Product/tomato.jpeg'),
    price: '10000 Đ',
  },
  {
    id: '8',
    title: 'Khoai tây',
    image:
    require('~/image/Product/photato.jpeg'),
    price: '30000 Đ',
  },
  {
    id: '9',
    title: 'Thịt bò loại 1',
    image:
    require('~/image/Product/beef.jpeg'),
    price: '130000 Đ',
  },
  {
    id: '10',
    title: 'Dưa chuột',
    image:
    require('~/image/Product/cucumber.jpeg'),
    price: '20000 Đ',
  },
];
export const DATA_COMMENT = [
  {
    avatar: require('~/image/ngoctrinh.jpg'),
    fullName: 'Phạm Anh',
    rate: 5,
    textComment:
      'Rau rất tươi và ngon, vận chuyển rất cẩn thận nên khi mình nhận hàng vẫn thấy tươi. Giá cả lại rất phải chăng.',
  },
  {
    avatar: require('~/image/ngoctrinh.jpg'),
    fullName: 'Phạm Anh',
    rate: 5,
    textComment:
      'Rau rất tươi và ngon, vận chuyển rất cẩn thận nên khi mình nhận hàng vẫn thấy tươi. Giá cả lại rất phải chăng.',
  },
  {
    avatar: require('~/image/ngoctrinh.jpg'),
    fullName: 'Phạm Anh',
    rate: 5,
    textComment:
      'Rau rất tươi và ngon, vận chuyển rất cẩn thận nên khi mình nhận hàng vẫn thấy tươi. Giá cả lại rất phải chăng.',
  },
];

const dataChat = [
  {type: 'reader'},
  {type: 'paymentDetail'},
  {type: 'paymentSuccess', content: 'Đơn hàng đã được tạo'},
  {
    type: 'userCreatePayment',
    content: 'Xa lach Da lat đã tạo một đơn hàng xà lách (#435435)',
  },
  {
    type: 'userCreateOrder',
    content: 'Xa lach Da lat đã tạo một đơn hàng xà lách (#435435)',
  },
  {
    type: 'receive',
    content: 'hi chao ban. ban muon j?',
    image: require('~/image/banner.png'),
  },
  {
    type: 'send',
    content: 'Xin chao ban',
    image: require('~/image/banner.png'),
  },
  {
    type: 'meCreateOrder',
    content: 'Phạm Anh đã tạo một đơn hàng xà lách (#435435)',
  },
];
