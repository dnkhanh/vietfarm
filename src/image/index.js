//Home

import Dacsan from './Home/dacsan.svg';
import Thit from './Home/thit.svg';
import Rau from './Home/rau.svg';
export {Dacsan, Thit, Rau};

// MAIN

import IconHome from './Main/home.svg';
import IconUHome from './Main/home_unfocus.svg';

import IconOffer from './Main/Offer.svg';
import IconUOffer from './Main/UOffer.svg';

import IconChat from './Main/message.svg';
import IconUChat from './Main/un_message.svg';

import IconUser from './Main/user.svg';
import IconUUser from './Main/un-user.svg';

export {
  IconHome,
  IconUHome,
  IconOffer,
  IconUOffer,
  IconChat,
  IconUChat,
  IconUser,
  IconUUser,
};

//Icon Base
import IconBell from './IconBase/bell.svg';
import IconSearch from './IconBase/search.svg';
import IconX from './IconBase/x.svg';
import IconCheck from './IconBase/check.svg';
import IconDot3 from './IconBase/dot_three.svg';
import IconDot3White from './IconBase/dot_three_white.svg';
import IconDown from './IconBase/down.svg';
import IconLeft from './IconBase/left.svg';
import IconRight from './IconBase/right.svg';

import IconTick from './IconBase/ic_check.svg';
import IconStar from './IconBase/star.svg';
import IconStarYellow from './IconBase/star_yellow.svg';
import IconShare from './IconBase/share.svg';
import IconShareWhite from './IconBase/share_white.svg';
//setting
import IconEdit from './IconBase/edit.svg';
import IconFileText from './IconBase/file-text.svg';
import IconLove from './IconBase/heart.svg';
import IconMessageCircle from './IconBase/message-circle.svg';
import IconShop from './IconBase/shop.svg';
import IconPeople from './IconBase/user.svg';
import IconVerify from './IconBase/ic_verify.svg';

//
import IconLocation from './IconBase/location.svg';
import IconLeft_White from './IconBase/left_white.svg';
import IconCamera from './IconBase/camera.svg';
import IconChevronRight from './IconBase/chevron-right.svg'
// login, register
import IconEye from './IconBase/eye.svg';
import IconLock from './IconBase/lock.svg';
import IconUserName from './IconBase/username.svg';
//chat

import IconPlus from './IconBase/plus.svg';
import IconPlusWhite from './IconBase/plus_white.svg';
import IconMinus from './IconBase/minus.svg';
import IconSend from './IconBase/send.svg';
import IconLogout from './IconBase/logout.svg';
export {
  IconBell,
  IconX,
  IconSearch,
  IconCheck,
  IconDot3,
  IconDown,
  IconLeft,
  IconRight,
  IconTick,
  IconEdit,
  IconFileText,
  IconLove,
  IconMessageCircle,
  IconShop,
  IconPeople,
  IconLocation,
  IconShare,
  IconShareWhite,
  IconLeft_White,
  IconDot3White,
  IconStar,
  IconStarYellow,
  IconVerify,
  IconPlus,
  IconSend,
  IconMinus,
  IconPlusWhite,
  IconCamera,
  IconEye,
  IconLock,
  IconUserName,
  IconChevronRight,
  IconLogout
};

import IconRateOrder from './Order/rate_order.svg';
import IconCreateOrder from './Order/create_order.svg';
import IconAddImage from './Order/add_image.svg';

export {IconRateOrder, IconCreateOrder, IconAddImage};
