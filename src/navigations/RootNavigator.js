import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Welcome from '../screen/Welcome';
import Main from '../screen/Main';
import Notification from '../screen/Notification';
import Edit from '../screen/Setting/Edit';
import ProductDetail from '~/screen/ProductDetail';
import ListMyStore from '~/screen/ListMyStore';
import MyStoreDetail from '~/screen/MyStoreDetail';
import Order from '~/screen/Order';
import Chat from '~/screen/Chat';
import Reviews from '~/screen/Reviews';
import Login from '~/screen/Login';
import Register from '~/screen/Register';
import Authphone from '~/screen/Authphone';
import SearchPlace from '~/screen/Search/place';
export const RootNavigator = createAppContainer(
  createStackNavigator(
    {
      Welcome: {
        screen: Welcome,
      },
      Main: {
        screen: Main,
      },
      Notification: {
        screen: Notification,
      },
      Edit: {
        screen: Edit,
      },
      ListMyStore: {
        screen: ListMyStore,
      },
      MyStoreDetail: {
        screen: MyStoreDetail,
      },
      ProductDetail: {
        screen: ProductDetail,
      },
      Chat: {
        screen: Chat,
      },
      Order: {
        screen: Order,
      },
      Reviews: {
        screen: Reviews,
      },
      Login: {
        screen: Login,
      },
      Register: {
        screen: Register,
      },
      Authphone:{
        screen: Authphone,
      },
      // search screen
      SearchPlace: {
        screen: SearchPlace,
      },
    },
    {
      headerMode: 'none',
      initialRouteName: 'Chat',
    },
  ),
);
