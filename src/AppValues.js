import {Dimensions, Platform} from 'react-native';

const {width: deviceWidth, height: deviceHeight} = Dimensions.get('window');

export const WIDTH_DEVICE = deviceWidth;

export const HEGHT_DEVICE = deviceHeight;
export const IS_PHONE_X =
  Platform.OS === 'ios' &&
  (WIDTH_DEVICE === 812 ||
    HEGHT_DEVICE === 812 ||
    WIDTH_DEVICE === 896 ||
    HEGHT_DEVICE === 896);

export const SHADOW_STYLE = {
  //Its for IOS
  shadowColor: '#000',
  shadowOffset: {width: 0, height: 1},
  shadowOpacity: 0.1,

  // its for Android
  elevation: 2,
  position: 'relative',
};

export const PADDING_HEADER = {
  paddingTop: IS_PHONE_X ? 40 : 15,
};
export const TYPE_MODAL_DANGER = 'danger';

export const TYPE_MODAL_WARNING = 'warning';

export const TYPE_MODAL_SUCCESS = 'success';

export const TYPE_MODAL_INFO = 'info';

export const COLOR_YELLOW = '#FFD600';

export const COLOR_DISABLE = '#D8DADD';

export const COLOR_BG_WHITE = '#fff';

export const COLOR_BG_RED = '#F63535';

export const COLOR_BG_GREY = '#D8DADD';

export const COLOR_BG_GREEN = '#00C95B';

export const COLOR_BG_GREEN_LIGHT = '#F5FFFA';

export const COLOR_BG_DARK_GREY = '#7B7B81';

export const COLOR_BG_DARK_GREY_LIGHT = '#9B9FAC';

export const COLOR_BG_BLACK = '#333537';

export const COLOR_BG_APP = '#FFFFFF';

export const COLOR_BORDER_TEXT_INPUT = '#D8DADD';

export const COLOR_GRAY = '#F5F6F8';

export const SIZE_H0 = 24;
export const SIZE_H1 = 22;
export const SIZE_H2 = 20;
export const SIZE_H3 = 18;
export const SIZE_H4 = 16;
export const SIZE_H5 = 14;
export const SIZE_H6 = 13;
export const SIZE_H7 = 12;

export const FONT_FAMILY =
  Platform.OS === 'android' ? 'Roboto-Regular' : 'Roboto';
