import { getString } from "~/Localizations/i18n";

const regex = {
  regexName: /^[a-zA-Z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêếìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẾẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴýÝỶỸửữựỳỵỷỹ][a-zA-Z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếẾỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴýÝỶỸửữựỳỵỷỹ]*([a-zA-Z0-9ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠếẾàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴýÝỶỸửữựỳỵỷỹ]*[\s][a-zA-Z0-9ÀÁÂếẾÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳýỵỷỹ]+)*$/,
  regexEmail: /^[A-Za-z0-9]([\\.-]?\w){1,62}[A-Za-z0-9]@[A-Za-z0-9]([\\.-]?\w+)*\.\w{0,}[A-Za-z0-9]$/,
  regexId: /^[a-zA-Z0-9]{3,24}$/,
  regexUserName: /^[a-zA-Z0-9@#$%^&*()\-_=+[\]{};:.’!?/\\~]{3,30}$/,
  regexPass: /^[a-zA-Z0-9@#$%^&*()\-_=+[\]{};:’!?/\\~]{4,36}$/,
  regexPhoneNumber: /^[0-9+]{9,12}$/,
  regexNumber: /^[0-9]{1,12}$/,
};
export const regexName = (Name) => {
  const name = (Name || '').trim();
  if (!name) return 'Vui lòng nhập tên.';
  if (
    name.length < 2 ||
    name.length > 50 ||
    !regex.regexName.test(name.trim())
  ) {
    return 'Tên không hợp lệ!';
  }
  return undefined;
};

export const regexPhone = (phone) => {
  if (!phone) return getString('register.error_phone_1');
  if (!regex.regexPhoneNumber.test(phone)) {
    return getString('register.error_phone_2');
  }
  return undefined;
};
export const regexEmail = (email) => {
  if (!email) return getString(register.error_mail_1);
  if (!regex.regexEmail.test(email)) {
    return getString('register.error_mail_2');
  }
  return undefined;
};
export const regexPassword = (Password) => {
  const password = (Password || '').trim();
  if (!password) return getString('register.error_pass_1');
  if (!regex.regexPass.test(password)) {
    return getString('register.error_pass_2');
  }
  return undefined;
};
