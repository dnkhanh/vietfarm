export default {
  //MAIN
  main: {
    home: 'Trang chủ',
    suggest: 'Đề xuất',
    chat: 'Tin nhắn',
    account: 'Tài khoản',
  },
  // HOME
  home: {
    title: 'Xin chào!\nBạn muốn đặt gì vào hôm nay?',
    textInput: 'Tìm sản phẩm, nhà sản xuất',
    rau: 'Rau Xanh',
    thit: 'Thịt',
    dacsan: 'Đặc Sản',
    productTop: 'Sản phẩm nổi bật',
  },
  // List CHAT
  list_chat: {
    text_order: 'Đơn Hàng',
    text_alert: 'Bạn: đã tạo một thanh toán',
    header: 'Tin nhắn',
  },

  chat: {},
  // Product Detail
  product_detail: {
    text_order: 'Đặt Hàng',
    text_more_rate_comment: 'Xem thêm đánh giá',
    text_rate: 'Đánh giá',
    text_rate_product: 'Đánh giá sản phẩm',
    text_detail_product: 'Chi tiết sản phẩm',
    text_go_store: 'Tới cửa hàng',
  },
  // Order
  order: {
    text_order: 'Đặt Hàng',
    text_more_rate_comment: 'Xem thêm đánh giá',
    text_rate: 'Đánh giá',
    text_rate_product: 'Đánh giá sản phẩm',
    text_detail_product: 'Chi tiết sản phẩm',
    text_go_store: 'Tới cửa hàng',
    text_continue: 'Tiếp tục',
  },
  //List My Store

  list_my_store: {
    header: 'Cửa hàng của bạn',
    btn_add: 'Thêm cửa hàng',
  },
  // REGIGTER && LOGIN
  register: {
    title: 'Đăng ký',
    phone: 'Số điện thoại',
    pass: 'Mật khẩu',
    repass: 'Nhập lại mật khẩu',
    submit: 'Đăng ký',
    haveacc: 'Bạn đã có tài khoản?',
    loginnow: 'Đăng nhập ngay',
    error : 'Vui lòng điền đầy đủ thông tin',
    error_phone_1: 'Vui lòng nhập Số điện thoại.',
    error_phone_2: 'Số điện thoại không hợp lệ!',
    error_mail_1:'Vui lòng nhập Email.',
    error_mail_2:'Email không hợp lệ!',
    error_mail_3:'Email đã được sử dụng',
    error_pass_1 : 'Vui lòng nhập mật khẩu',
    error_pass_2: 'Mật khẩu không hợp lệ',
    error_repass: 'Mật khẩu nhập lại không khớp với mật khẩu',
  },
  login: {
    title: 'Đăng nhập',
    username: 'Email / SĐT / Hoặc tài khoản',
    pass: 'Mật khẩu',
    submit: 'Đăng nhập',
    forgotpass: 'Bạn quên mật khẩu?',
    nothaveacc: 'Bạn chưa có tài khoản',
    regnow:'Đăng ký ngay',
    error: 'Vui lòng điền đầy đủ thông tin',
    error_username_1: 'Tài khoản không đúng định dạng!',
    error_username_2: 'Tài khoản không tồn tại!',
    error_username_3: 'Tài khoản phải là Email',
    error_pass_1: 'Mật khẩu sai!',
    error_pass_2: 'Vui lòng nhập mật khẩu',
  },
  // PROFILE SETTING

  setting: {
    header: 'Tài khoản',
    your_store: 'Cửa Hàng Của Bạn',
    mng_acc: 'Quản lý tài khoản',
    mng_orders: 'Quản lý đơn hàng',
    fav_products: 'Sản phẩm yêu thích',
    your_rate: 'Đánh giá của bạn',
    info_address: 'Sổ địa chỉ',
    info_payments: 'Thông tin thanh toán',
    change_language: 'Thay đổi ngôn ngữ',
    text_support: 'HỖ TRỢ',
    text_contact: 'Liên hệ',
    text_report: 'Báo lỗi hệ thống',
  },

  // PROFILE EDIT
  edit_profile: {
    title_change: 'Thay đổi mật khẩu',
    text_fullname: 'Họ và tên',
    text_email: 'Email',
    text_phone: 'Số điện thoại',
    text_passold: 'Mật khẩu cũ',
    text_passnew: 'Mật khẩu mới',
    header: 'Thông tin cá nhân',
    btn_update: 'Lưu thay đổi',
  },

  //SUGGEST

  suggest: {
    textInput: 'Tìm sản phẩm, nhà sản xuất',
    hasVerify: 'Đã update giấy tờ',
    noVerify: 'Chưa update giấy tờ',
    place: 'Địa điểm',
    reputation: 'Độ uy tín',
  },

  notification: {
    header: 'Thông báo của tôi',
  },
};
